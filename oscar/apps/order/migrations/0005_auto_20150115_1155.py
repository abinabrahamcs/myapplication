# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('order', '0004_auto_20141218_1900'),
    ]

    operations = [
        migrations.AlterField(
            model_name='order',
            name='status',
            field=models.CharField(default=b'Shipped', max_length=100, verbose_name='Status', blank=True, choices=[(b'Shipped', b'Shipped'), (b'Pending', b'Pending'), (b'delivered', b'delivered')]),
        ),
    ]
