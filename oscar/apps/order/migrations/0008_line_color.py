# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('order', '0007_auto_20150202_1211'),
    ]

    operations = [
        migrations.AddField(
            model_name='line',
            name='color',
            field=models.CharField(max_length=200, null=True, verbose_name='Color', blank=True),
            preserve_default=True,
        ),
    ]
