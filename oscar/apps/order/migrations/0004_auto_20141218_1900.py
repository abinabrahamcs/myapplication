# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('order', '0003_line_champion'),
    ]

    operations = [
        migrations.AlterField(
            model_name='line',
            name='champion',
            field=models.ForeignKey(related_name=b'champion_order_lines', blank=True, to='userprofile.UserProfile', null=True),
        ),
    ]
