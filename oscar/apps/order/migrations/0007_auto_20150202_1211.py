# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('order', '0006_auto_20150119_1230'),
    ]

    operations = [
        migrations.AlterField(
            model_name='order',
            name='status',
            field=models.CharField(default=b'Shipped', max_length=100, verbose_name='Status', blank=True, choices=[(b'Shipped', b'Shipped'), (b'Pending', b'Pending'), (b'Returned', b'Returned'), (b'Failure', b'Failure')]),
        ),
    ]
