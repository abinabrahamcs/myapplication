# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('order', '0008_line_color'),
    ]

    operations = [
        migrations.AddField(
            model_name='line',
            name='size',
            field=models.CharField(max_length=200, null=True, verbose_name='Size', blank=True),
            preserve_default=True,
        ),
    ]
