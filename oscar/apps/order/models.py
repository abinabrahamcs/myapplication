from oscar.core.loading import is_model_registered
from oscar.apps.order.abstract_models import *  # noqa
from oscar.apps.address.abstract_models import (AbstractShippingAddress,
                                                AbstractBillingAddress)
from django.utils.translation import ugettext_lazy as _                                                
                                                
from django.db import models                                                

__all__ = []


if not is_model_registered('order', 'Order'):
    class Order(AbstractOrder):
        pass

    __all__.append('Order')


if not is_model_registered('order', 'OrderNote'):
    class OrderNote(AbstractOrderNote):
        pass

    __all__.append('OrderNote')


if not is_model_registered('order', 'CommunicationEvent'):
    class CommunicationEvent(AbstractCommunicationEvent):
        pass

    __all__.append('CommunicationEvent')


if not is_model_registered('order', 'ShippingAddress'):
    class ShippingAddress(AbstractShippingAddress):
        pass

    __all__.append('ShippingAddress')


if not is_model_registered('order', 'BillingAddress'):
    class BillingAddress(AbstractBillingAddress):
        pass

    __all__.append('BillingAddress')


if not is_model_registered('order', 'Line'):
    class Line(AbstractLine):
        champion = models.ForeignKey('userprofile.UserProfile',
                            related_name='champion_order_lines', null=True, blank=True)
        color = models.CharField(_("Color"), max_length=200, null=True, blank=True)                    
        size = models.CharField(_("Size"), max_length=200, null=True, blank=True)
        @property                    
        def get_order_earn(self):
            """
            Get ordered product earn priceS
            """
            return self.product.champion_earn * self.quantity

    __all__.append('Line')


if not is_model_registered('order', 'LinePrice'):
    class LinePrice(AbstractLinePrice):
        pass

    __all__.append('LinePrice')


if not is_model_registered('order', 'LineAttribute'):
    class LineAttribute(AbstractLineAttribute):
        pass

    __all__.append('LineAttribute')


if not is_model_registered('order', 'ShippingEvent'):
    class ShippingEvent(AbstractShippingEvent):
        pass

    __all__.append('ShippingEvent')


if not is_model_registered('order', 'ShippingEventType'):
    class ShippingEventType(AbstractShippingEventType):
        pass

    __all__.append('ShippingEventType')


if not is_model_registered('order', 'PaymentEvent'):
    class PaymentEvent(AbstractPaymentEvent):
        pass

    __all__.append('PaymentEvent')


if not is_model_registered('order', 'PaymentEventType'):
    class PaymentEventType(AbstractPaymentEventType):
        pass

    __all__.append('PaymentEventType')


if not is_model_registered('order', 'OrderDiscount'):
    class OrderDiscount(AbstractOrderDiscount):
        pass

    __all__.append('OrderDiscount')
