from oscar.core.loading import is_model_registered
from apps.userprofile import *
from oscar.apps.basket.abstract_models import (
    AbstractBasket, AbstractLine, AbstractLineAttribute)
from django.db import models
from django.utils.translation import ugettext_lazy as _

__all__ = [
    'InvalidBasketLineError',
]


class InvalidBasketLineError(Exception):
    pass


if not is_model_registered('basket', 'Basket'):
    class Basket(AbstractBasket):
        pass

    __all__.append('Basket')


if not is_model_registered('basket', 'Line'):
    class Line(AbstractLine):
        champion = models.ForeignKey('userprofile.UserProfile',
                            related_name='champion_basket_lines', null=True, blank=True)
        color = models.CharField(_("Color"), max_length=200, null=True, blank=True)
        size = models.CharField(_("Size"), max_length=200, null=True, blank=True)
                            
        @property
        def get_available_bulk_rate(self):
            """
            Available bulk rate
            """
            price = None
            if self.product.bulk_range.all():
                for range in  self.product.bulk_range.all():
                    if range.quantity_from and range.quantity_to:
                        if range.quantity_from <= self.quantity and range.quantity_to >= self.quantity:
                            price = range.price
                    else:
                        if range.quantity_from <= self.quantity:
                            price = range.price                        
            return price      
        
        @property
        def get_total_available_bulk_rate(self):
            """
            Available total bulk rate
            """
            price = self.get_available_bulk_rate * self.quantity
            return price

    __all__.append('Line')


if not is_model_registered('basket', 'LineAttribute'):
    class LineAttribute(AbstractLineAttribute):
        pass

    __all__.append('LineAttribute')
