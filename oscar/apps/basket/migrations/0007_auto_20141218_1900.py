# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('basket', '0006_auto_20141203_1209'),
    ]

    operations = [
        migrations.AlterField(
            model_name='line',
            name='champion',
            field=models.ForeignKey(related_name=b'champion_basket_lines', blank=True, to='userprofile.UserProfile', null=True),
        ),
    ]
