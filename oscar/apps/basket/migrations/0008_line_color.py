# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('basket', '0007_auto_20141218_1900'),
    ]

    operations = [
        migrations.AddField(
            model_name='line',
            name='color',
            field=models.CharField(max_length=200, null=True, verbose_name='Color', blank=True),
            preserve_default=True,
        ),
    ]
