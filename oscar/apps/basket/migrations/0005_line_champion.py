# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('userprofile', '0003_userprofile_location'),
        ('basket', '0004_auto_20141007_2032'),
    ]

    operations = [
        migrations.AddField(
            model_name='line',
            name='champion',
            field=models.ForeignKey(related_name=b'basket_lines', blank=True, to='userprofile.UserProfile', null=True),
            preserve_default=True,
        ),
    ]
