from django.views.generic import TemplateView, RedirectView
from django.core.urlresolvers import reverse
from django.views.generic import ListView, View
from oscar.core.loading import get_class, get_model
from django.utils.translation import ugettext_lazy as _

Product = get_model('catalogue', 'product')
get_product_search_handler_class = get_class(
    'catalogue.search_handlers', 'get_product_search_handler_class')

class HomeView(ListView):
    """
    This is the home page and will typically live at /
    """
    template_name = 'promotions/indorso_home.html'
    model = Product
    context_object_name = "products"
    
    def get_context_data(self, **kwargs):
        # Call the base implementation first to get a context
        context = super(HomeView, self).get_context_data(**kwargs)
        # Add in a QuerySet of all the books
        context['products'] = Product.objects.all()
        return context
    
class RecordClickView(RedirectView):
    """
    Simple RedirectView that helps recording clicks made on promotions
    """
    permanent = False
    model = None

    def get_redirect_url(self, **kwargs):
        try:
            prom = self.model.objects.get(pk=kwargs['pk'])
        except self.model.DoesNotExist:
            return reverse('promotions:home')

        if prom.promotion.has_link:
            prom.record_click()
            return prom.link_url
        return reverse('promotions:home')
