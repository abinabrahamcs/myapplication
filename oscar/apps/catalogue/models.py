from __future__ import division
import django
from decimal import *
from django.utils.translation import ugettext_lazy as _, pgettext_lazy

"""
Vanilla product models
"""
from oscar.core.loading import is_model_registered
from oscar.apps.catalogue.abstract_models import *  # noqa
from apps.userprofile.models import *
from apps.champions.models import *
from ckeditor.fields import RichTextField

from oscar.core.loading import get_model, get_class
Country = get_model('address', 'Country')

Selector = get_class('partner.strategy', 'Selector')
strategy = Selector().strategy()

__all__= ['ProductAttributesContainer']


if not is_model_registered('catalogue', 'ProductClass'):
    class ProductClass(AbstractProductClass):
        pass

    __all__.append('ProductClass')


if not is_model_registered('catalogue', 'Category'):
    class Category(AbstractCategory):
        pass

    __all__.append('Category')


if not is_model_registered('catalogue', 'ProductCategory'):
    class ProductCategory(AbstractProductCategory):
        pass

    __all__.append('ProductCategory')

class Colors(models.Model):
    """
    Product colors
    """
    
    name = models.CharField(_("Color"), max_length=200)
    
    def __unicode__(self):
        return self.name

class Sizes(models.Model):
    """
    Product size
    """
    
    name = models.CharField(_("Size"), max_length=200)
    
    def __unicode__(self):
        return self.name


if not is_model_registered('catalogue', 'Product'):
    class Product(AbstractProduct):
        """
        Additional fields
        """
        product_video_id = models.CharField(_("Product Video Id"),
                                        max_length=200, null=True, blank=True)
        innovator = models.ForeignKey(UserProfile, related_name = "product_innovaters")
        champions = models.ManyToManyField(UserProfile,
                    related_name = "champion_licenses", null=True, blank=True)
        bulk_range = models.ManyToManyField(BulkProductRange,
                    related_name = "bulk_prices", null=True, blank=True)
        colors = models.ManyToManyField(Colors,
                    related_name = "product_colors", null=True, blank=True)
        sizes = models.ManyToManyField(Sizes,
                    related_name = "product_sizes", null=True, blank=True)          
        description2 = RichTextField(_('Description 2'), blank=True)
        available_countries = models.ManyToManyField(Country,
                    related_name = "available_countries", null=True, blank=True)
        
        @property
        def total_indorsed_license(self):
            """
            Total number of licenses indorsed
            """
            if self.champions.all():
                list = self.champions.all()
                count = len(list)
            else:
                count = 0
            return count
        
        @property
        def available_licence(self):
            """
            Available license
            """
            license_count = 50 - self.total_indorsed_license
            if license_count > 0:
                return license_count
            return 0
        
        @property
        def has_license(self):
            """
            Less than 50 licenses of that product have been taken/indorsed
            """
            if self.total_indorsed_license < 50:
                status = True
            else:
                status = False                    
            return status
        
        @property
        def champion_earn(self):
            """
            Get champion commition price
            """
            info = strategy.fetch_for_product(self)
            if info.price.is_tax_known:
                price = info.price.incl_tax
            else:
                price = info.price.excl_tax
            commition = Decimal(info.stockrecord.commision_price/100)
            return commition * price
        
        @property
        def is_product_expired(self):
            """
            Checking product is expired
            """
            
            import datetime            
            today = datetime.date.today()
            if self.has_stockrecords:
                stockrecord = self.stockrecords.all()[0]
                available_date = stockrecord.available_date.date()
                if today <= available_date:
                     status = True
                else:
                    status = False
            else:
                status = False                    
            return status
        
        @property
        def check_expire_today(self):
            """
            Check product expire date today
            """
            no_of_days = None
            import datetime            
            today = datetime.date.today()
            status = None
            if self.has_stockrecords:
                stockrecord = self.stockrecords.all()[0]
                available_date = stockrecord.available_date.date()
                no_of_days = available_date - today
                if no_of_days == datetime.timedelta(0, 0):
                    status = True
                else:
                    status = False
            return status
        
        
        @property
        def days_to_go(self):
            """
            Product expire days
            """
            no_of_days = None
            import datetime
            today = datetime.date.today()            
            if self.has_stockrecords:
                stockrecord = self.stockrecords.all()[0]
                available_date = stockrecord.available_date.date()
                no_of_days = available_date - today
#                if no_of_days == datetime.timedelta(0, 0):
#                    balance_time = stockrecord.available_date - datetime.datetime.now()
#                    return available_date
            return no_of_days.days
        
        @property
        def get_available_date(self):
            """
            """      
            if self.has_stockrecords:
                stockrecord = self.stockrecords.all()[0]
                available_date = stockrecord.available_date
            else:
                available_date = None
            print     
            return available_date
        
        def search(self, values, searchFor):
            """
              search for a product id in offers dictionary
            """
            for k in values:
                #for v in values[k]:
                if long(searchFor) in values[k]:
                    return k
            return None
            

    __all__.append('Product')
    
class ShippingPrices(models.Model):
    """
    Shipping price model
    """
    product = models.ForeignKey(Product, related_name = "product_shipping_prices")
    shipping_from = models.ForeignKey(Country, related_name = "product_shipping_from")
    shipping_to = models.ForeignKey(Country, related_name = "product_shipping_to")
    shipping_price = models.DecimalField(_('Price'), decimal_places=2, max_digits=12)
    
if not is_model_registered('catalogue', 'ProductRecommendation'):
    class ProductRecommendation(AbstractProductRecommendation):
        pass

    __all__.append('ProductRecommendation')


if not is_model_registered('catalogue', 'ProductAttribute'):
    class ProductAttribute(AbstractProductAttribute):
        pass

    __all__.append('ProductAttribute')


if not is_model_registered('catalogue', 'ProductAttributeValue'):
    class ProductAttributeValue(AbstractProductAttributeValue):
        pass

    __all__.append('ProductAttributeValue')


if not is_model_registered('catalogue', 'AttributeOptionGroup'):
    class AttributeOptionGroup(AbstractAttributeOptionGroup):
        pass

    __all__.append('AttributeOptionGroup')


if not is_model_registered('catalogue', 'AttributeOption'):
    class AttributeOption(AbstractAttributeOption):
        pass

    __all__.append('AttributeOption')


if not is_model_registered('catalogue', 'Option'):
    class Option(AbstractOption):
        pass

    __all__.append('Option')


if not is_model_registered('catalogue', 'ProductImage'):
    class ProductImage(AbstractProductImage):
        pass

    __all__.append('ProductImage')


if django.VERSION < (1, 7):
    from . import receivers  # noqa