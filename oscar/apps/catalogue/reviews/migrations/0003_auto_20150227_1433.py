# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import oscar.core.validators


class Migration(migrations.Migration):

    dependencies = [
        ('reviews', '0002_auto_20150218_1100'),
    ]

    operations = [
        migrations.AlterField(
            model_name='productreview',
            name='title',
            field=models.CharField(blank=True, max_length=255, null=True, verbose_name='Title', validators=[oscar.core.validators.non_whitespace]),
        ),
    ]
