# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('catalogue', '0004_auto_20150120_1702'),
    ]

    operations = [
        migrations.AddField(
            model_name='product',
            name='product_video_id',
            field=models.CharField(max_length=200, null=True, verbose_name='Product Video Id', blank=True),
            preserve_default=True,
        ),
    ]
