# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('address', '0002_auto_20150226_1242'),
        ('catalogue', '0016_auto_20150224_1803'),
    ]

    operations = [
        migrations.CreateModel(
            name='ShippingPrices',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('shipping_price', models.DecimalField(verbose_name='Price', max_digits=12, decimal_places=2)),
                ('product', models.ForeignKey(related_name=b'product_shipping_prices', to='catalogue.Product')),
                ('shipping_from', models.ForeignKey(related_name=b'product_shipping_from', to='address.Country')),
                ('shipping_to', models.ForeignKey(related_name=b'product_shipping_to', to='address.Country')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
    ]
