# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('catalogue', '0011_product_colors'),
    ]

    operations = [
        migrations.AlterField(
            model_name='product',
            name='description2',
            field=models.TextField(verbose_name='Description 2', blank=True),
        ),
    ]
