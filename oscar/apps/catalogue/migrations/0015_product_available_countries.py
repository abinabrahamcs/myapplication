# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('address', '0001_initial'),
        ('catalogue', '0014_product_sizes'),
    ]

    operations = [
        migrations.AddField(
            model_name='product',
            name='available_countries',
            field=models.ManyToManyField(related_name=b'available_countries', null=True, to='address.Country', blank=True),
            preserve_default=True,
        ),
    ]
