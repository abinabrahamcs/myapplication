# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('userprofile', '0011_auto_20150119_1256'),
        ('catalogue', '0002_product_innovator'),
    ]

    operations = [
        migrations.AddField(
            model_name='product',
            name='champions',
            field=models.ManyToManyField(to='userprofile.UserProfile', null=True, blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='product',
            name='innovator',
            field=models.ForeignKey(related_name=b'product_innovaters', to='userprofile.UserProfile'),
        ),
    ]
