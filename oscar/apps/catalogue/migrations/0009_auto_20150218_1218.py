# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import ckeditor.fields


class Migration(migrations.Migration):

    dependencies = [
        ('catalogue', '0008_auto_20150212_1134'),
    ]

    operations = [
        migrations.AlterField(
            model_name='product',
            name='description2',
            field=ckeditor.fields.RichTextField(verbose_name='Description 2', blank=True),
        ),
    ]
