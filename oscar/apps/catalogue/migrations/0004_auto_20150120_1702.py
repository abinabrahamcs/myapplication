# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('catalogue', '0003_auto_20150119_1608'),
    ]

    operations = [
        migrations.AlterField(
            model_name='product',
            name='champions',
            field=models.ManyToManyField(related_name=b'champion_licenses', null=True, to=b'userprofile.UserProfile', blank=True),
        ),
    ]
