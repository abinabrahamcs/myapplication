# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('champions', '0009_bulkproductrange'),
        ('catalogue', '0005_product_product_video_id'),
    ]

    operations = [
        migrations.AddField(
            model_name='product',
            name='bulk_range',
            field=models.ManyToManyField(related_name=b'bulk_prices', null=True, to='champions.BulkProductRange', blank=True),
            preserve_default=True,
        ),
    ]
