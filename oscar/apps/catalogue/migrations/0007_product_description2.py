# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime


class Migration(migrations.Migration):

    dependencies = [
        ('catalogue', '0006_product_bulk_range'),
    ]

    operations = [
        migrations.AddField(
            model_name='product',
            name='description2',
            field=models.TextField(default=datetime.date(2015, 2, 6), verbose_name='Description 2', blank=True),
            preserve_default=False,
        ),
    ]
