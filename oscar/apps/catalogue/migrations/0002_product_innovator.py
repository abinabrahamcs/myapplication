# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('userprofile', '0002_auto_20141121_0947'),
        ('catalogue', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='product',
            name='innovator',
            field=models.ForeignKey(default=1, to='userprofile.UserProfile'),
            preserve_default=False,
        ),
    ]
