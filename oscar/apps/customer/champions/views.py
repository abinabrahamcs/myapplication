from django.contrib import messages
from django.core.exceptions import (
    ObjectDoesNotExist, MultipleObjectsReturned, PermissionDenied)
from django.core.urlresolvers import reverse
from django.http import Http404
from django.shortcuts import get_object_or_404, redirect
from django.views.generic import (ListView, CreateView, UpdateView, DeleteView,
                                  View, FormView)
from django.utils.translation import ugettext_lazy as _

from oscar.apps.customer.mixins import PageTitleMixin
from oscar.core.loading import get_classes, get_model
from oscar.core.utils import redirect_to_referrer, safe_referrer

from oscar.apps.order.models import Line    
    
class ChampionsCommisionProductsCalculation(PageTitleMixin, ListView):
    context_object_name = active_tab = "commission"
    template_name = 'customer/champions/commission.html'
    page_title = _('Champions Commission')

    def get_queryset(self):
        line = Line.objects.filter(champion=self.request.user.user_profile)
        return line