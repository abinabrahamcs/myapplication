import logging

from django.core.mail import EmailMessage, EmailMultiAlternatives
from django.core.urlresolvers import reverse
from django.conf import settings
from django.contrib.auth.tokens import default_token_generator
from django.utils.http import urlsafe_base64_encode
from django.utils.encoding import force_bytes
from django.conf import settings
from django.template.loader import render_to_string

from django.template import Context
from django.template.loader import get_template

from oscar.core.loading import get_model

CommunicationEvent = get_model('order', 'CommunicationEvent')
Email = get_model('customer', 'Email')


class Dispatcher(object):
    def __init__(self, logger=None):
        if not logger:
            logger = logging.getLogger(__name__)
        self.logger = logger

    # Public API methods

    def dispatch_direct_messages(self, recipient, messages):
        """
        Dispatch one-off messages to explicitly specified recipient(s).
        """
        if messages['subject'] and messages['body']:
            self.send_email_messages(recipient, messages)

    def dispatch_order_messages(self, order, messages, event_type=None,
                                **kwargs):
        """
        Dispatch order-related messages to the customer
        """
        if order.is_anonymous:
            if 'email_address' in kwargs:
                self.send_email_messages(kwargs['email_address'], messages)
            elif order.guest_email:
                self.send_email_messages(order.guest_email, messages)
            else:
                return
        else:
            self.dispatch_order_champion_messages(order, messages)
            self.dispatch_user_messages(order.user, messages)

        # Create order communications event for audit
        if event_type is not None:
            CommunicationEvent._default_manager.create(
                order=order, event_type=event_type)

    def dispatch_user_messages(self, user, messages):
        """
        Send messages to a site user
        """
        admin_email = settings.DEFAULT_FROM_EMAIL
        if messages['subject'] and (messages['body'] or messages['html']):
            self.send_user_email_messages(user, messages)
            if 'Confirmation of order' in messages['subject']:
                self.send_user_email_messages(user, messages,admin_email)
        if messages['sms']:
            self.send_text_message(user, messages['sms'])
            if 'Confirmation of order' in messages['subject']:
                self.send_user_email_messages(user, messages['sms'],admin_email)

    # Internal

    def send_user_email_messages(self, user, messages, admin_email=''):
        """
        Sends message to the registered user / customer and collects data in
        database
        """
        if admin_email != '':
            messages['subject'] = "Admin "+ messages['subject']
            email = self.send_email_messages(admin_email, messages)
        else:
            if not user.email:
                self.logger.warning("Unable to send email messages as user #%d has"
                                    " no email address", user.id)
                return

            email = self.send_email_messages(user.email, messages)

        # Is user is signed in, record the event for audit
        if email and user.is_authenticated():
            Email._default_manager.create(user=user,
                                          subject=email.subject,
                                          body_text=email.body,
                                          body_html=messages['html'])

    def send_email_messages(self, recipient, messages):
        """
        Plain email sending to the specified recipient
        """
        if hasattr(settings, 'OSCAR_FROM_EMAIL'):
            from_email = settings.OSCAR_FROM_EMAIL
        else:
            from_email = None

        # Determine whether we are sending a HTML version too
        if messages['html']:
            if messages['subject'].startswith('Admin'):
#                template = get_template('customer/emails/commtype_order_placed_body.html')
#                context = Context({'order': order})
#                content = template.render(context)
#                msg = EmailMessage(subject, content, from_email=from_email, to=[recipient,])
#                msg.send()
                email = EmailMultiAlternatives(messages['subject'],
                                               messages['admin_body'],
                                               from_email=from_email,
                                               to=[recipient])
            else:
                email = EmailMultiAlternatives(messages['subject'],
                                               messages['body'],
                                               from_email=from_email,
                                               to=[recipient])
                email.attach_alternative(messages['html'], "text/html")
#                email = EmailMessage(messages['subject'],
#                                 messages['body'],
#                                 from_email=from_email,
#                                 to=[obj.champion.user.email for obj in self.order.lines.all()])
        else:
            email = EmailMessage(messages['subject'],
#                                 messages['body'],
                                 messages['admin_body'],
                                 from_email=from_email,
                                 to=[recipient])
        self.logger.info("Sending email to %s" % recipient)
        email.send()

        return email

    def send_text_message(self, user, event_type):
        raise NotImplementedError
    
    # added for champions section(both functions below)
    def dispatch_order_champion_messages(self, order, messages):
        """
         Created for champion email purpose. Call goes to below function.
        """
        admin_email = settings.DEFAULT_FROM_EMAIL
#        champmail_list = [obj.champion.user.email for obj in order.lines.all()]
#        innovmail_list = [ob.product.innovator.user.email for ob in order.lines.all()]
#        innovmail_list = list(set(innovmail_list))
        for obj in order.lines.all():
            if messages['subject'] and (messages['body'] or messages['html']):
                self.send_champion_email_messages(order, obj, messages)
        for obj in order.lines.all():
            if messages['subject'] and (messages['body'] or messages['html']):
                self.send_inno_email_messages(order, obj, messages)
        
    def send_champion_email_messages(self, order, obj, messages):
        
        if hasattr(settings, 'OSCAR_FROM_EMAIL'):
            from_email = settings.OSCAR_FROM_EMAIL
        else:
            from_email = None
        message = render_to_string('customer/emails/commtype_order_placed_bodychamp.txt',{'order':order})
        email = EmailMessage(messages['subject'],
                               message,
                               from_email=from_email,
                               to=[obj.champion.user.email,])
        email.send()
        return email
    
    def send_inno_email_messages(self, order, obj, messages):
        
        if hasattr(settings, 'OSCAR_FROM_EMAIL'):
            from_email = settings.OSCAR_FROM_EMAIL
        else:
            from_email = None
        if obj.product.innovator.user.email and from_email:
            message = render_to_string('customer/emails/commtype_order_placed_bodychamp.txt',{'order':order})
            email = EmailMessage(messages['subject'],
                                   message,
                                   from_email=from_email,
                                   to=[obj.product.innovator.user.email,])
            email.send()
        else:
            email = None
        return email


def get_password_reset_url(user, token_generator=default_token_generator):
    """
    Generate a password-reset URL for a given user
    """
    kwargs = {
        'token': token_generator.make_token(user),
        'uidb64': urlsafe_base64_encode(force_bytes(user.id)),
    }
    return reverse('password-reset-confirm', kwargs=kwargs)


def normalise_email(email):
    """
    The local part of an email address is case-sensitive, the domain part
    isn't.  This function lowercases the host and should be used in all email
    handling.
    """
    clean_email = email.strip()
    if '@' in clean_email:
        local, host = clean_email.split('@')
        return local + '@' + host.lower()
    return clean_email


