# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('customer', '0002_communicationeventtype_email_admin_body_template'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='communicationeventtype',
            name='email_admin_body_template',
        ),
    ]
