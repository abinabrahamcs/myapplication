# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('customer', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='communicationeventtype',
            name='email_admin_body_template',
            field=models.TextField(null=True, verbose_name=b'Email Admin Body Template', blank=True),
            preserve_default=True,
        ),
    ]
