from django import forms
from oscar.core.loading import get_model
from django.contrib.auth.forms import AuthenticationForm
from django.utils.translation import ugettext_lazy as _

from oscar.apps.address.forms import AbstractAddressForm
from oscar.apps.customer.utils import normalise_email
from oscar.core.compat import get_user_model

from oscar.views.generic import PhoneNumberMixin

User = get_user_model()
Country = get_model('address', 'Country')
ShippingPrices = get_model('catalogue', 'ShippingPrices')


class ShippingAddressForm(PhoneNumberMixin, AbstractAddressForm):

    def __init__(self, *args, **kwargs):
        self.basket = kwargs.pop('basket', None)
        super(ShippingAddressForm, self).__init__(*args, **kwargs)
        self.adjust_country_field()
        for field in self.fields:
            if field in self.errors:
                self.fields[field].widget.attrs['class']='form-control warning'
                self.fields[field].widget.attrs['placeholder']=self._errors[field].as_text()
            else:
                self.fields[field].widget.attrs['class']='form-control'

    def adjust_country_field(self):
        countries = Country._default_manager.filter(
            is_shipping_country=True)

        # No need to show country dropdown if there is only one option
        if len(countries) == 1:
#            self.fields.pop('country', None)
            self.fields['country'].queryset = countries
            self.fields['country'].widget.attrs['class'] = 'form-control'
#            self.instance.country = countries[0]
        else:
            self.fields['country'].queryset = countries
            self.fields['country'].empty_label = None
            self.fields['country'].widget.attrs['class'] = 'form-control'
            
    def clean_country(self):
        error_product = []
        shipping_price_error = []
        for line in self.basket.lines.all():            
            
            # checking shipping price available selected country?  
            available_shipping_prices = list(line.product.product_shipping_prices.all().values_list('shipping_to__printable_name', flat=True))
            if available_shipping_prices:
                if not str(self.cleaned_data.get('country', None)) in available_shipping_prices:
                    shipping_price_error.append(line.product.title)
            
            # checking product is available selected country?     
            available_countries = list(line.product.available_countries.all())     
            if available_countries:
                if not self.cleaned_data.get('country', None) in available_countries:
                    error_product.append(line.product.title)                    
                    
        if shipping_price_error:
            products = ','.join(error_product)
            raise forms.ValidationError("%s does't have a stock price"% (products))
        if error_product:
            products = ','.join(error_product)
            raise forms.ValidationError("%s is not available in %s"% (products,self.cleaned_data.get('country', None)))
        return self.cleaned_data.get('country', None)
    
    class Meta:
        model = get_model('order', 'shippingaddress')
        exclude = ('user', 'search_text')


class GatewayForm(AuthenticationForm):
    username = forms.EmailField(label=_("My email address is"))
    GUEST, NEW, EXISTING = 'anonymous', 'new', 'existing'
    CHOICES = (
        (GUEST, _('I am a new customer and want to checkout as a guest')),
        (NEW, _('I am a new customer and want to create an account '
                'before checking out')),
        (EXISTING, _('I am a returning customer, and my password is')))
    options = forms.ChoiceField(widget=forms.widgets.RadioSelect,
                                choices=CHOICES, initial=GUEST)

    def clean_username(self):
        return normalise_email(self.cleaned_data['username'])

    def clean(self):
        if self.is_guest_checkout() or self.is_new_account_checkout():
            if 'password' in self.errors:
                del self.errors['password']
            if 'username' in self.cleaned_data:
                email = normalise_email(self.cleaned_data['username'])
                if User._default_manager.filter(email__iexact=email).exists():
                    msg = "A user with that email address already exists"
                    self._errors["username"] = self.error_class([msg])
            return self.cleaned_data
        return super(GatewayForm, self).clean()

    def is_guest_checkout(self):
        return self.cleaned_data.get('options', None) == self.GUEST

    def is_new_account_checkout(self):
        return self.cleaned_data.get('options', None) == self.NEW


# The BillingAddress form is in oscar.apps.payment.forms
