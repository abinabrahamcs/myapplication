# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('partner', '0004_auto_20141208_1817'),
    ]

    operations = [
        migrations.AlterField(
            model_name='stockrecord',
            name='commision_price',
            field=models.PositiveIntegerField(default=0, verbose_name='Commision Price'),
        ),
    ]
