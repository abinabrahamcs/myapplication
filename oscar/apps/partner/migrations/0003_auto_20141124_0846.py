# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime


class Migration(migrations.Migration):

    dependencies = [
        ('partner', '0002_auto_20141007_2032'),
    ]

    operations = [
        migrations.AddField(
            model_name='stockrecord',
            name='available_date',
            field=models.DateTimeField(default=datetime.date(2014, 11, 24), verbose_name='License available date'),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='stockrecord',
            name='commision_price',
            field=models.DecimalField(default=2.2, verbose_name='Commision Price', max_digits=12, decimal_places=2),
            preserve_default=False,
        ),
    ]
