# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('partner', '0005_auto_20141208_1830'),
    ]

    operations = [
        migrations.AlterField(
            model_name='stockrecord',
            name='commision_price',
            field=models.DecimalField(default=0, verbose_name='Commision Price', max_digits=10, decimal_places=2),
        ),
    ]
