import django
from django.db import models
from django.utils.translation import ugettext_lazy as _

from oscar.core.loading import is_model_registered
from oscar.apps.address.abstract_models import AbstractPartnerAddress
from oscar.apps.partner.abstract_models import (
    AbstractPartner, AbstractStockRecord, AbstractStockAlert)

__all__ = []


if not is_model_registered('partner', 'Partner'):
    class Partner(AbstractPartner):
        pass

    __all__.append('PartnerAddress')


if not is_model_registered('partner', 'PartnerAddress'):
    class PartnerAddress(AbstractPartnerAddress):
        pass

    __all__.append('PartnerAddress')


if not is_model_registered('partner', 'StockRecord'):
    class StockRecord(AbstractStockRecord):
        """
        Additional fields
        """
        available_date = models.DateTimeField(_("License available date"))
        commision_price = models.DecimalField(_("Commision Price"), default=0,decimal_places=2,max_digits=10)

    __all__.append('StockRecord')


if not is_model_registered('partner', 'StockAlert'):
    class StockAlert(AbstractStockAlert):
        pass

    __all__.append('StockAlert')


if django.VERSION < (1, 7):
    from . import receivers  # noqa
