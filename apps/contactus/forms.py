from django import forms
from django.conf import settings
from django.core.mail import send_mail
from django.template import loader
from django.template import RequestContext
from django.contrib.sites.models import RequestSite
from django.contrib.sites.models import Site

from models import Contact

class ContactForm(forms.ModelForm):
    class Meta:
        model = Contact
        
    def __init__(self, request=None, *args, **kwargs):
        if request is None:
            raise TypeError("Keyword argument 'request' must be supplied")
        super(ContactForm, self).__init__(*args, **kwargs)
        for field in self.fields:
            self.fields[field].widget.attrs['class']='form-control'
        self.request = request

    from_email = settings.DEFAULT_FROM_EMAIL

    recipient_list = [mail_tuple for mail_tuple in settings.MANAGERS]

    subject_template_name = "contactus/contact_form_subject.txt"

    template_name = 'contactus/contact_request.txt'

    def send_mail(self):
        send_mail(fail_silently = False, **self.get_message_dict())

    def message(self):
        """
          Renders the body of the message to a string.
        """
        if callable(self.template_name):
            template_name = self.template_name()
        else:
            template_name = self.template_name
        return loader.render_to_string(template_name,
                                            self.get_context())

    def subject(self):
        """
          Renders the subject of the message to a string.
        """
        subject = loader.render_to_string(self.subject_template_name,
                                                        self.get_context())
        return ''.join(subject.splitlines())

    def get_context(self):
        """
        Return the context used to render the templates for the email
        subject and body.
        """
        if not self.is_valid():
            raise ValueError("Cannot generate Context from invalid contact form")
        if Site._meta.installed:
            site = Site.objects.get_current()
        else:
            site = RequestSite(self.request)
        return RequestContext(self.request,
                              dict(self.cleaned_data,
                                   site=site))

    def get_message_dict(self):
        """
          Generate various parts of the message.
        """
        if not self.is_valid():
            raise "Message cannot sent to an invalid form"
        message_dict = {}
        for message_part in ('from_email', 'message', 'recipient_list', 'subject'):
            attr = getattr(self, message_part)
            message_dict[message_part] = attr() if callable(attr) else attr
        return message_dict
