from django.conf.urls import patterns
from django.conf.urls import url

from views import ContactFormView

urlpatterns = patterns('',
                    url(r'^$',
                           ContactFormView.as_view(),
                           name='contact_form'),
                           )