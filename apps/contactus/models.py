from django.db import models
from django.utils.translation import ugettext_lazy as _

from utility_files.db import DateBaseModel

class Contact(DateBaseModel):
    name = models.CharField(_("Name"), max_length=100)
    email= models.EmailField(_("Email"), max_length=30)
    phone = models.CharField(_("Telephone"), max_length=100)
    comments = models.TextField(_('Comments'))

    def __unicode__(self):
        return self.name

    class Meta:
        verbose_name = _("Contact")
        verbose_name_plural = _("Contact")
