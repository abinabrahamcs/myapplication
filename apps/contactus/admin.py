from django.contrib import admin
from models import Contact

class ContactAdmin(admin.ModelAdmin):

    search_fields = ['name']
    fields = ['name','email','phone','comments']

admin.site.register(Contact,ContactAdmin)