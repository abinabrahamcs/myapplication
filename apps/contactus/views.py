from django.core.urlresolvers import reverse
from django.views.generic.edit import FormView
from django.contrib import messages

from forms import ContactForm

class ContactFormView(FormView):
    """
    This view can render contact form to model and Send mail def
    """
    form_class = ContactForm
    template_name = 'contactus/contact_us.html'

    def form_valid(self, form):
        form.save()
        form.send_mail()
        messages.success(self.request, "Thank you for contacting us.\
                                We'll reply to you soon", extra_tags='msg')
        return super(ContactFormView, self).form_valid(form)

    def get_form_kwargs(self):
        kwargs = super(ContactFormView, self).get_form_kwargs()
        kwargs.update({'request': self.request})
        return kwargs

    def get_success_url(self):
        # return success url
        return reverse('contact_form')

#@csrf_exempt
#def contact_form_view(request):
#    current_site = Site.objects.get_current()
#    context = RequestContext(request)
#    form = ContactForm()
#    if request.method == 'POST':
#        form = ContactForm(request.POST)
#        if form.is_valid():
#            form.save()
#            name = request.POST['name']
#            email = request.POST['email']
#            phone = request.POST['phone']
#            comments = request.POST['comments']
#            t = loader.get_template('contact_request.txt')
#            c= Context({'name' : name, 'email' : email, 'phone' : phone,'comments':comments,})
#            send_mail('[%s] %s' % (current_site.name, 'New Contact Request'), t.render(c), settings.DEFAULT_FROM_EMAIL, [email,], fail_silently = False)
#            form = ContactForm(auto_id=False)
#        else:
#            print form.errors
#    else:
#         form = ContactForm()
#    return render_to_response('contactus/contact_us.html', {'form': form}, context)
