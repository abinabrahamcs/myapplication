# -*- coding: utf-8 -*-
from oscar.core.loading import get_model      
from geoip import geolite2  

Country = get_model('address', 'Country')

class LocationMiddleware(object):

    def process_request(self, request):
        """
        Get current country
        """
#        ip = request.META.get('REMOTE_ADDR')
        ip = "202.88.237.198"       
        match = geolite2.lookup(ip)
        if match is not None:
            current_country = match.country
        else:
            current_country = None
        try:
            country_object = Country.objects.get(iso_3166_1_a2=current_country)
        except:
            country_object = None
        request.country_object = country_object