from django.db import models
from django.utils.translation import ugettext_lazy as _
from django.contrib.sites.models import Site
from ckeditor.fields import RichTextField
from oscar.core.loading import get_class, get_model
Product = get_model('catalogue', 'product')


class CommonData(models.Model):
    """
      Model for common settings (the things are unique for this site)
    """
    site = models.OneToOneField(Site)
    logo = models.ImageField(_('Logo'),upload_to='Logo/')
    footer_text = models.CharField(_('Footer Text'), max_length=100, blank=True, null=True)
    
    # static pages contents
    
    privacy_policy = RichTextField(_('Privacy policy content'))
    terms_of_use = RichTextField(_('Terms of use content'))
    return_policy = RichTextField(_('Return policy content'))
    about_us = RichTextField(_('About us content'))
    faq = RichTextField(_('FAQ content'))
    how_it_works = RichTextField(_('How it works'))
    
    # social network icons on the footer.
    
    linkedin_url = models.CharField(_('LInkedin link'), max_length=100, blank=True, null=True)
    facebk_url = models.CharField(_('Facebook link'), max_length=100, blank=True, null=True)
    twit_url = models.CharField(_('Twitter link'), max_length=100, blank=True, null=True)
    gplus_url = models.CharField(_('Gplus link'), max_length=100, blank=True, null=True)
    pin_url = models.CharField(_('Pinterest link'), max_length=100, blank=True, null=True)
    product_a = models.OneToOneField(Product,related_name='pr_a',blank=True, null=True )
    product_b = models.OneToOneField(Product,related_name='pr_b',blank=True, null=True)
    product_c = models.OneToOneField(Product,related_name='pr_c',blank=True, null=True)

    class Meta:
        verbose_name = _('Common Details')
        verbose_name_plural = _('Common Details')

    def __unicode__(self):
        return u'%s' %(self.site)

class FooterLinks(models.Model):
    """
      Models for handling footer links like About us etc..
    """
    title = models.CharField(_("Title"), max_length=300, blank=True, null=True)
    URL = models.URLField(_("URL"), blank=True,null=True,help_text=_('If page is not specified, footer link will redirect to given Url'))
    sort_order = models.IntegerField(_("Sort order"), default=0)
    published = models.BooleanField(_("Published"), default=True)

    class Meta:
        verbose_name = _("Footer Link")
        verbose_name_plural = _("Footer Links")
        ordering = ('sort_order',)

    def __unicode__(self):
        return "%s" % self.title

    @property
    def get_url(self):
        if self.page_link:
            return self.page_link.get_absolute_url()
        elif self.URL:
            return self.URL
        return '#'

    @classmethod
    def get_published(self):
        return self.objects.filter(published=True)