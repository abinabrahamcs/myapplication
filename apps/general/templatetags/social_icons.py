from django import template
from django.contrib.sites.models import Site
from apps.general.models import CommonData

register = template.Library()

@register.assignment_tag()
def list_common_details():
    current_site = Site.objects.get_current()
    try:
        common_details = CommonData.objects.get(site = current_site.id)
    except:
        common_details = ''
    return common_details

@register.filter(name='multiply')
def multiply(value, arg):
    return value*arg