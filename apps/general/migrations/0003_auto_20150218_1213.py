# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import ckeditor.fields


class Migration(migrations.Migration):

    dependencies = [
        ('general', '0002_auto_20150218_1100'),
    ]

    operations = [
        migrations.AlterField(
            model_name='commondata',
            name='about_us',
            field=ckeditor.fields.RichTextField(verbose_name='About us content'),
        ),
        migrations.AlterField(
            model_name='commondata',
            name='privacy_policy',
            field=ckeditor.fields.RichTextField(verbose_name='Privacy policy content'),
        ),
        migrations.AlterField(
            model_name='commondata',
            name='return_policy',
            field=ckeditor.fields.RichTextField(verbose_name='Return policy content'),
        ),
        migrations.AlterField(
            model_name='commondata',
            name='terms_of_use',
            field=ckeditor.fields.RichTextField(verbose_name='Terms of use content'),
        ),
    ]
