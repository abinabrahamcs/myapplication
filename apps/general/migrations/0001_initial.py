# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('sites', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='CommonData',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('logo', models.ImageField(upload_to=b'Logo/', verbose_name='Logo')),
                ('privacy_policy', models.TextField(verbose_name='Privacy policy content')),
                ('terms_of_use', models.TextField(verbose_name='Terms of use content')),
                ('return_policy', models.TextField(verbose_name='Return policy content')),
                ('about_us', models.TextField(verbose_name='About us content')),
                ('linkedin_url', models.CharField(max_length=100, null=True, verbose_name='LInkedin link', blank=True)),
                ('facebk_url', models.CharField(max_length=100, null=True, verbose_name='Facebook link', blank=True)),
                ('twit_url', models.CharField(max_length=100, null=True, verbose_name='Twitter link', blank=True)),
                ('gplus_url', models.CharField(max_length=100, null=True, verbose_name='Gplus link', blank=True)),
                ('site', models.OneToOneField(to='sites.Site')),
            ],
            options={
                'verbose_name': 'Common settings',
                'verbose_name_plural': 'Common settings',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='FooterLinks',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(max_length=300, null=True, verbose_name='Title', blank=True)),
                ('URL', models.URLField(help_text='If page is not specified, footer link will redirect to given Url', null=True, verbose_name='URL', blank=True)),
                ('sort_order', models.IntegerField(default=0, verbose_name='Sort order')),
                ('published', models.BooleanField(default=True, verbose_name='Published')),
            ],
            options={
                'ordering': ('sort_order',),
                'verbose_name': 'Footer Link',
                'verbose_name_plural': 'Footer Links',
            },
            bases=(models.Model,),
        ),
    ]
