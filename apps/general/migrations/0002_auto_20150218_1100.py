# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('general', '0001_initial'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='commondata',
            options={'verbose_name': 'Common Details', 'verbose_name_plural': 'Common Details'},
        ),
    ]
