# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('general', '0004_auto_20150218_1406'),
    ]

    operations = [
        migrations.AddField(
            model_name='commondata',
            name='footer_text',
            field=models.CharField(max_length=100, null=True, verbose_name='Footer Text', blank=True),
            preserve_default=True,
        ),
    ]
