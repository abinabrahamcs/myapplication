# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('general', '0006_auto_20150228_1051'),
    ]

    operations = [
        migrations.AddField(
            model_name='commondata',
            name='pin_url',
            field=models.CharField(max_length=100, null=True, verbose_name='Pinterest link', blank=True),
            preserve_default=True,
        ),
    ]
