# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('catalogue', '0017_shippingprices'),
        ('general', '0005_commondata_footer_text'),
    ]

    operations = [
        migrations.AddField(
            model_name='commondata',
            name='product_a',
            field=models.OneToOneField(related_name=b'pr_a', null=True, blank=True, to='catalogue.Product'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='commondata',
            name='product_b',
            field=models.OneToOneField(related_name=b'pr_b', null=True, blank=True, to='catalogue.Product'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='commondata',
            name='product_c',
            field=models.OneToOneField(related_name=b'pr_c', null=True, blank=True, to='catalogue.Product'),
            preserve_default=True,
        ),
    ]
