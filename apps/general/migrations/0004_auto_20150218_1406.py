# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime
import ckeditor.fields


class Migration(migrations.Migration):

    dependencies = [
        ('general', '0003_auto_20150218_1213'),
    ]

    operations = [
        migrations.AddField(
            model_name='commondata',
            name='faq',
            field=ckeditor.fields.RichTextField(default=datetime.date(2015, 2, 18), verbose_name='FAQ content'),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='commondata',
            name='how_it_works',
            field=ckeditor.fields.RichTextField(default=datetime.date(2015, 2, 18), verbose_name='How it works'),
            preserve_default=False,
        ),
    ]
