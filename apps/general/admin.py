from django.contrib import admin
from apps.general.models import *

admin.site.register(CommonData)
admin.site.register(FooterLinks)
