from django.conf.urls import patterns, url
from views import *
from django.contrib.auth.decorators import login_required

urlpatterns = patterns("",
url(r'^user-registration/$', UserRegistration.as_view(),
                                            name="user-registration"),
url(r'^accounts/storename/$', UserAccountStoreUpdation.as_view(),
                                            name="account_store"),
url(r'^accounts/updateprofile/$', UserAccountProfilePicUpdation.as_view(),
                                            name="account_profile"),
url(r'^accounts/updatebio/$', UserAccountBioUpdation.as_view(),
                                            name="account_bio"),
url(r'^accounts/skypeid/$', UserAccountSkypeUpdation.as_view(),
                                            name="account_skype"),
url(r'^accounts/phone_number/$', UserAccountPhoneUpdation.as_view(),
                                            name="account_phone_number"),
url(r'^accounts/website/$', UserAccountWebsiteUpdation.as_view(),
                                            name="account_website"),
url(r'^accounts/first_name/$', UserAccountFirstNameUpdation.as_view(),
                                            name="first_name"),
url(r'^accounts/last_name/$', UserAccountLastNameUpdation.as_view(),
                                            name="last_name"),
 url(r'^change-password-user/$',
                login_required(ChangePasswordView.as_view()),
                name='change-password-user'),
)