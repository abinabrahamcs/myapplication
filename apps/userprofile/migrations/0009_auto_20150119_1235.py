# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('userprofile', '0008_auto_20150114_1245'),
    ]

    operations = [
        migrations.AlterField(
            model_name='userprofile',
            name='location',
            field=models.CharField(max_length=100, null=True, verbose_name='Location', blank=True),
        ),
        migrations.AlterField(
            model_name='userprofile',
            name='type',
            field=models.CharField(default=b'Enduser', max_length=100, choices=[(b'Innovator', b'Innovator'), (b'Champion', b'Champion'), (b'Enduser', b'Enduser')]),
        ),
    ]
