# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('userprofile', '0005_auto_20141230_1445'),
    ]

    operations = [
        migrations.AddField(
            model_name='userprofile',
            name='paypal_id',
            field=models.EmailField(max_length=75, null=True, verbose_name='PaypalEmail-ID', blank=True),
            preserve_default=True,
        ),
    ]
