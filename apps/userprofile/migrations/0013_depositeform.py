# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('userprofile', '0012_userprofile_timeline_image'),
    ]

    operations = [
        migrations.CreateModel(
            name='DepositeForm',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('file', models.FileField(upload_to=b'DepositeForm')),
                ('user_profile', models.ForeignKey(related_name=b'champion_deposite_form', to='userprofile.UserProfile')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
    ]
