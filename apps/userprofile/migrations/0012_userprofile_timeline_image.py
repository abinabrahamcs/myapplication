# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('userprofile', '0011_auto_20150119_1256'),
    ]

    operations = [
        migrations.AddField(
            model_name='userprofile',
            name='timeline_image',
            field=models.ImageField(upload_to=b'user_timeline', null=True, verbose_name='Time Line Image', blank=True),
            preserve_default=True,
        ),
    ]
