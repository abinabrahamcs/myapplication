# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('userprofile', '0007_auto_20150109_1755'),
    ]

    operations = [
        migrations.AddField(
            model_name='userprofile',
            name='phone_number',
            field=models.CharField(max_length=12, null=True, verbose_name='Phone number', blank=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='userprofile',
            name='skype_id',
            field=models.CharField(max_length=100, null=True, verbose_name='Skype ID', blank=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='userprofile',
            name='website',
            field=models.CharField(max_length=100, null=True, verbose_name='Company website', blank=True),
            preserve_default=True,
        ),
    ]
