# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('userprofile', '0013_depositeform'),
    ]

    operations = [
        migrations.AddField(
            model_name='userprofile',
            name='link1',
            field=models.CharField(max_length=100, null=True, verbose_name='Link URL 1', blank=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='userprofile',
            name='link2',
            field=models.CharField(max_length=100, null=True, verbose_name='Link URL 2', blank=True),
            preserve_default=True,
        ),
    ]
