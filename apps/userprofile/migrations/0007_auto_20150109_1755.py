# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('userprofile', '0006_userprofile_paypal_id'),
    ]

    operations = [
        migrations.RenameField(
            model_name='userprofile',
            old_name='paypal_id',
            new_name='paypal_email',
        ),
    ]
