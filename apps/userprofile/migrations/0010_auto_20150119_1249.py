# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('userprofile', '0009_auto_20150119_1235'),
    ]

    operations = [
        migrations.AlterField(
            model_name='userprofile',
            name='type',
            field=models.CharField(default=b'Enduser', max_length=100, null=True, choices=[(b'Innovator', b'Innovator'), (b'Champion', b'Champion'), (b'Enduser', b'Enduser')]),
        ),
    ]
