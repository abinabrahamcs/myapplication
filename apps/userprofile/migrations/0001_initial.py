# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='UserProfile',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('type', models.CharField(max_length=100, choices=[(b'Innovator', b'Innovator'), (b'Champion', b'Champion'), (b'Enduser', b'Enduser')])),
                ('image', models.ImageField(upload_to=b'userprofile', null=True, verbose_name=' User Image', blank=True)),
                ('storename', models.CharField(max_length=100, null=True, verbose_name='Store Name', blank=True)),
                ('bio', models.TextField(null=True, verbose_name='Description', blank=True)),
                ('user', models.OneToOneField(related_name=b'user_profile', to=settings.AUTH_USER_MODEL)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
    ]
