from django.shortcuts import render, redirect
from django.views.generic import TemplateView, View
from django.views.generic.edit import FormView, UpdateView, CreateView
from forms import *
from django.shortcuts import render_to_response
from django.template import RequestContext
from django.http import HttpResponse
from django.contrib.sites.models import get_current_site
import json
from oscar.core.loading import (
    get_class, get_profile_class, get_classes, get_model)
from django.views import generic
PasswordChangeForm = get_class('customer.forms', 'PasswordChangeForm')
from django.utils.translation import ugettext_lazy as _
from django.core.urlresolvers import reverse, reverse_lazy

from django.shortcuts import get_object_or_404, redirect
from django.views import generic
from django.core.urlresolvers import reverse, reverse_lazy
from django.core.exceptions import ObjectDoesNotExist
from django import http
from django.contrib import messages
from django.utils.translation import ugettext_lazy as _
from django.contrib.auth import logout as auth_logout, login as auth_login
from django.contrib.sites.models import get_current_site
from django.conf import settings
from django.http import HttpResponse
from django.http import HttpResponseRedirect

from django.contrib.auth.models import User
from oscar.core.utils import safe_referrer
from oscar.views.generic import PostActionMixin
from oscar.apps.customer.utils import get_password_reset_url
from django.contrib.auth import authenticate, login
from oscar.core.loading import (
    get_class, get_profile_class, get_classes, get_model)
from oscar.core.compat import get_user_model
from django.core.mail import EmailMessage
from django.template.loader import render_to_string

CommunicationEventType = get_model('customer', 'CommunicationEventType')

Dispatcher = get_class('customer.utils', 'Dispatcher')
PageTitleMixin, RegisterUserMixin = get_classes(
    'customer.mixins', ['PageTitleMixin', 'RegisterUserMixin'])
    
    
class UserRegistration(CreateView):
    """
    User registraion 
    """    
    template_name = "user_profile/registration.html"
    form_class = UserForm
    model = User
    success_url = '/'
    
    def get(self, request, *args, **kwargs):
        """
        Handles GET requests and instantiates blank versions of the form
        and its inline formsets.
        """
        self.object = None
        form_class = self.get_form_class()
        form = self.get_form(form_class)
        profile_form = UserProfileForm()
        return self.render_to_response(
            self.get_context_data(form=form,
                                  profile_form=profile_form))
                                  
    def post(self, request, *args, **kwargs):
        """
        Handles POST requests, instantiating a form instance and its inline
        formsets with the passed POST variables and then checking them for
        validity.
        """
	
        self.object = None
        form_class = self.get_form_class()
        form = self.get_form(form_class)
        profile_form = UserProfileForm(self.request.POST, self.request.FILES)
        if (form.is_valid() and profile_form.is_valid()):
            return self.form_valid(form, profile_form)
        else:
            return self.form_invalid(form, profile_form)
        
    def form_valid(self, form, profile_form):
        """
        Called if all forms are valid. Creates a Recipe instance along with
        associated Ingredients and Instructions and then redirects to a
        success page.
        """
        
        self.object = form.save()
        p = profile_form.save(commit=False)
        p.paypal_email= self.object.email            
        p.user = self.object
        p.storename = self.object.username
        p.save()
        messages.success(
            self.request,
            _("Successfully Registered"))
        subject = _('New User Registration')
        message = render_to_string('user_profile/user_reg.txt',{ 'object':self.object})
        EmailMessage(subject, message, settings.DEFAULT_FROM_EMAIL, [settings.DEFAULT_FROM_EMAIL]).send()
        
        subject_to_user = _('Successfully Registered')
        message_to_user = render_to_string('user_profile/user_reg_touser.txt',{ 'object':self.object, 'site': get_current_site(self.request),})
        EmailMessage(subject_to_user, message_to_user, settings.DEFAULT_FROM_EMAIL, [self.object.email]).send()
        
        user = self.object
        user.backend = 'django.contrib.auth.backends.ModelBackend'
        login(self.request, user)
        
        return redirect('/products-indorse/')
#        return self.render_to_response(self.get_context_data(form=form,
#            profile_form=profile_form, success=True))
        
    def form_invalid(self, form, profile_form):
        """
        Called if a form is invalid. Re-renders the context data with the
        data-filled forms and errors.
        """
        return self.render_to_response(self.get_context_data(form=form,
            profile_form=profile_form))


class UserAccountStoreUpdation(View):
    """
    User registraion 
    """    
    template_name = "champions/dashboard/account.html"
    

    def post(self,request):        
        data = {}
        errors = {}
        try:
            storename = request.POST.get('storename')
        except:
            storename = ''
        if not storename:
            errors['error'] = "storename not found"
            data = json.dumps(errors)
            response_kwargs = {}
            response_kwargs['status'] = 400
            response_kwargs['content_type'] = 'application/json'
            return HttpResponse(data, **response_kwargs)
        if (' ' in storename) == True:
            errors['error'] = "Please choose storename without unnecessary spaces"
            data = json.dumps(errors)
            response_kwargs = {}
            response_kwargs['status'] = 400
            response_kwargs['content_type'] = 'application/json'
            return HttpResponse(data, **response_kwargs)
            
        try:
            user_id = request.POST.get('user_id')
        except:
            user_id = ''
        user_profile = self.get_user(user_id)
        user_profile.storename = storename
        user_profile.save()
        data['status_su'] = 'Successfully updated'
        data['storename'] = storename
        return HttpResponse(json.dumps(data))
    
    def get_user(self, user_id):
        """
        Get the user from UserProfile model
       """
        try:
            user_profile = UserProfile.objects.get(user__id=user_id)
        except: 
            user_profile = None
        return user_profile
    
    
class UserAccountProfilePicUpdation(View):
    """
    User registraion 
    """    
    template_name = "champions/dashboard/account.html"
    success_url = reverse_lazy('champion-account')
    

    def post(self,request): 
        try:
            profile = request.FILES['uploadField']
        except:
            profile = ''
        user_profile = self.get_user(self.request.user.id)
        user_profile.image = profile
        user_profile.save()
        messages.success(self.request, _("Profile updated"))
        return redirect(self.success_url)
    
    def get_user(self, user_id):
        """
        Get the user from UserProfile model
       """
        try:
            user_profile = UserProfile.objects.get(user__id=user_id)
        except: 
            user_profile = None
        return user_profile
    
    
class UserAccountBioUpdation(View):
    """
    User registraion 
    """    
    template_name = "champions/dashboard/account.html"
    

    def post(self,request):        
        data = {}
        try:
            biofield = request.POST.get('biofield')
        except:
            biofield = ''
        try:
            user_id = request.POST.get('user_id')
        except:
            user_id = ''
        user_profile = self.get_user(user_id)
        user_profile.bio = biofield
        user_profile.save()
        data['status_su'] = 'Successfully updated'
        return HttpResponse(json.dumps(data))
    
    def get_user(self, user_id):
        """
        Get the user from UserProfile model
       """
        try:
            user_profile = UserProfile.objects.get(user__id=user_id)
        except: 
            user_profile = None
        return user_profile
    
    
class UserAccountSkypeUpdation(View):
    """
    User registraion 
    """    
    template_name = "champions/dashboard/account.html"
    

    def post(self,request):        
        data = {}
        try:
            skype_id = request.POST.get('skype_id')
        except:
            skype_id = ''
        try:
            user_id = request.POST.get('user_id')
        except:
            user_id = ''
        user_profile = self.get_user(user_id)
        user_profile.skype_id = skype_id
        user_profile.save()
        data['status_su'] = 'Successfully updated'
        data['skype'] = skype_id
        return HttpResponse(json.dumps(data))
    
    def get_user(self, user_id):
        """
        Get the user from UserProfile model
       """
        try:
            user_profile = UserProfile.objects.get(user__id=user_id)
        except: 
            user_profile = None
        return user_profile
    
    
class UserAccountPhoneUpdation(View):
    """
    User registraion 
    """    
    template_name = "champions/dashboard/account.html"
    

    def post(self,request):        
        data = {}
        try:
            phone_number = request.POST.get('phone_number')
        except:
            phone_number = ''
        try:
            user_id = request.POST.get('user_id')
        except:
            user_id = ''
        user_profile = self.get_user(user_id)
        user_profile.phone_number = phone_number
        user_profile.save()
        data['status_su'] = 'Successfully updated'
        data['phone_number'] = phone_number
        return HttpResponse(json.dumps(data))
    
    def get_user(self, user_id):
        """
        Get the user from UserProfile model
       """
        try:
            user_profile = UserProfile.objects.get(user__id=user_id)
        except: 
            user_profile = None
        return user_profile
    
    
class UserAccountWebsiteUpdation(View):
    """
    User registraion 
    """    
    template_name = "champions/dashboard/account.html"
    

    def post(self,request):        
        data = {}
        try:
            website = request.POST.get('website')
            user_id = request.POST.get('user_id')
        except:
            website = None
            user_id = None
        user_profile = self.get_user(user_id)
        user_profile.website = website
        user_profile.save()
        data['status_su'] = 'Successfully updated'
        return HttpResponse(json.dumps(data))
    
    def get_user(self, user_id):
        """
        Get the user from UserProfile model
       """
        try:
            user_profile = UserProfile.objects.get(user__id=user_id)
        except: 
            user_profile = None
        return user_profile
    
    
class UserAccountFirstNameUpdation(View):
    """
    User registraion 
    """    
    template_name = "champions/dashboard/account.html"
    

    def post(self,request):        
        data = {}
        try:
            first_name = request.POST.get('first_name')
            user_id = request.POST.get('user_id')
        except:
            first_name = None
            user_id = None
        user_profile = User.objects.get(id=user_id)
        user_profile.first_name = first_name
        user_profile.save()
        data['status_su'] = 'Successfully updated'
        data['firstname'] = first_name
        return HttpResponse(json.dumps(data))
    
    
class UserAccountLastNameUpdation(View):
    """
    User registraion 
    """    
    template_name = "champions/dashboard/account.html"
    

    def post(self,request):        
        data = {}
        try:
            last_name = request.POST.get('last_name')
            user_id = request.POST.get('user_id')
        except:
            last_name = None
            user_id = None
        user_profile = User.objects.get(id=user_id)
        user_profile.last_name = last_name
        user_profile.save()
        data['status_su'] = 'Successfully updated'
        data['lastname'] = last_name
        return HttpResponse(json.dumps(data))
    
        
class ChangePasswordView(PageTitleMixin, generic.FormView):
    form_class = PasswordChangeForm
    template_name = "champions/dashboard/password_change.html"
    communication_type_code = 'PASSWORD_CHANGED'
    page_title = _('Change Password')
    active_tab = 'profile'
    success_url = reverse_lazy('home_login')

    def get_form_kwargs(self):
        kwargs = super(ChangePasswordView, self).get_form_kwargs()
        kwargs['user'] = self.request.user
        return kwargs

    def form_valid(self, form):
        form.save()
        messages.success(self.request, _("Password updated"))

        ctx = {
            'user': self.request.user,
            'site': get_current_site(self.request),
            'reset_url': get_password_reset_url(self.request.user),
        }
        msgs = CommunicationEventType.objects.get_and_render(
            code=self.communication_type_code, context=ctx)
        Dispatcher().dispatch_user_messages(self.request.user, msgs)

        return redirect(self.success_url)