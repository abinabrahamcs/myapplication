from django.db import models
from django.utils.translation import ugettext_lazy as _
from oscar.models.fields import UppercaseCharField, PhoneNumberField
from decimal import *

TYPECHOICES = (
    ('Innovator','Innovator'),
    ('Champion','Champion'),
    ('Enduser','Enduser'),
)

class UserProfile(models.Model):
    """
    User profile model
    """
    user = models.OneToOneField('auth.User', related_name="user_profile")
    type = models.CharField(choices=TYPECHOICES, null=True,blank=True,
                                        default="Enduser", max_length=100)
    image = models.ImageField(_(" User Image"), upload_to ="uploads_profile",
                                    null=True, blank=True, max_length=100)
    timeline_image = models.ImageField(_("Time Line Image"), upload_to ="user_timeline",
                                    null=True, blank=True, max_length=100)                                
    storename = models.CharField(_("Store Name"), max_length = 100,
                                    null=True, blank=True, unique=True)
    location = models.CharField(_("Location"), max_length=100, null=True, blank=True,)
    follows = models.ManyToManyField('self', related_name='follower', 
                                    symmetrical=False, null=True, blank=True,)
    bio = models.TextField(_('Description'), null=True, blank=True)
    paypal_email = models.EmailField(_('PaypalEmail-ID'), null=True, blank=True)
    skype_id = models.CharField(_("Skype ID"), max_length=100, null=True, blank=True)
    phone_number = models.CharField(_("Phone number"), max_length=12, null=True, blank=True)
    website = models.CharField(_("Company website"), max_length=100, blank=True, null=True)
    link1 = models.CharField(_("Link URL 1"), max_length=100, blank=True, null=True)
    link2 = models.CharField(_("Link URL 2"), max_length=100, blank=True, null=True)
    
    
    def __unicode__(self):
        return self.user.username
    
    @property
    def champions_indorsed_product_quota(self):
        """
        Get total count of the indorsed products
        """
        if self.champion_licenses.all():
            quota = self.champion_licenses.all().count()
        else:
            quota = 0
        return quota
    
    @property
    def champions_exceeded_quota(self):
        """
        Champions havent exceeded their 25 quota
        """        
        if self.champions_indorsed_product_quota < 25:
            status = True
        else:
            status = False
        return  status
    
    @property
    def champion_total_earnings_balance(self):
        """
        Champions total earning balance
        """
        price = Decimal(0)
        if self.champion_order_lines.all():
            for license in self.champion_order_lines.all():
                price+=license.get_order_earn
        return price
    
    @property
    def champion_total_units_sold(self):
        """
        Units sold by champions
        """
        count = 0
        if self.champion_order_lines.all():
            for license in self.champion_order_lines.all():
                count+=license.quantity            
        return count
    
    @property
    def get_total_indorsments_count(self):
        
        """
        Get number of follows
        """
        return self.champion_licenses.all().count()
    
    
    @property
    def get_no_of_follows(self):
        
        """
        Get number of follows
        """
        return self.follows.all().count()
    
    @property
    def get_no_of_followers(self):
        
        """
        Get number of followers
        """
        return self.follower.all().count()
    
class DepositeForm(models.Model):
    """
    Bank Transfer file model
    """
    user_profile = models.ForeignKey(UserProfile, related_name="champion_deposite_form")
    file = models.FileField(upload_to = "DepositeForm")
