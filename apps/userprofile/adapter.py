from allauth.socialaccount.adapter import DefaultSocialAccountAdapter
from allauth.account.adapter import get_adapter as get_account_adapter
from oscar.core.loading import get_class, get_model

userprofile = get_model('userprofile', 'UserProfile')

class CustomSocialAccountAdapter(DefaultSocialAccountAdapter):

    

    def save_user(self, request, sociallogin, form=None):
        """
        Saves a newly signed up social login. In case of auto-signup,
        the signup form is not available.
        """
        u = sociallogin.user
        u.set_unusable_password()
        if form:
            get_account_adapter().save_user(request, u, form)
        else:
            get_account_adapter().populate_username(request, u)
        sociallogin.save(request)
        
        
        try:
            user_profile = userprofile.objects.get(user=u)
        except:
            user_profile = userprofile()
        user_profile.user = u 
        user_profile.storename = u.username
        user_profile.save()
        return u