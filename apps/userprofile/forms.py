from django import forms
from django.utils.translation import ugettext_lazy as _
from django.contrib.auth.models import *
from models import *
from django.contrib.auth.hashers import make_password
from django.utils.html import strip_tags

class UserForm(forms.ModelForm):
    """
    User
    """
    password1 = forms.CharField(label='Password', max_length=8, widget=forms.PasswordInput())
    password2 = forms.CharField(label='Confirm Password', max_length=8,
                                widget=forms.PasswordInput())
    class Meta:
        model = User
        fields = ("username", "email")
        
    def __init__(self, *args, **kwrgs):
        super(UserForm, self).__init__(*args, **kwrgs)        
        for field in self.fields:            
            if field in self.errors:
                self.fields[field].widget.attrs['class']='form-control warning'
                self.fields[field].widget.attrs['placeholder']=field        
            else:
                self.fields[field].widget.attrs['class']='form-control'
        self.fields['password1'].widget.attrs['placeholder']='Password length 8'
    
    def clean_password2(self):
        password1 = self.cleaned_data.get('password1')
        password2 = self.cleaned_data.get('password2')
        if password1 and password2:
            if password1 != password2:
                raise forms.ValidationError("The two password fields didn't match.")
            return password2
        
    def clean_email(self):
        if self.cleaned_data["email"] == "":
            raise forms.ValidationError('This field is required.')
        try:
            User.objects.get(email = self.cleaned_data['email'])
            raise forms.ValidationError('This email is already taken.')    
        except User.DoesNotExist:
            return self.cleaned_data['email']        
        
    def save(self, commit=True):
        user = super(UserForm, self).save(commit=False)
        user.set_password(self.cleaned_data["password2"])
        if commit:
            user.save()
        return user
    
    
class UserProfileForm(forms.ModelForm):
    """
    User profile form
    """
    read = forms.BooleanField(required=True)
    
    class Meta:
        model = UserProfile
        exclude = ['user']
        
    def __init__(self, *args, **kwrgs):
        super(UserProfileForm, self).__init__(*args, **kwrgs)
        for field in self.fields:            
            if field == 'image':
                self.fields[field].widget.attrs['class']='form-control'
            elif field in self.errors:
                if field == 'bio':
                    self.fields[field].widget.attrs['class']='form-control warning texthgt'
                else:    
                    self.fields[field].widget.attrs['class']='form-control warning'
                    self.fields[field].widget.attrs['placeholder']=self._errors[field].as_text()
            else:
                if field == 'bio':
                    self.fields[field].widget.attrs['class']='form-control texthgt'
                else:    
                    self.fields[field].widget.attrs['class']='form-control'
            
                
    def clean_storename(self):
        if 'type' in self.cleaned_data:
            if self.cleaned_data["type"] == "Champion":                
                if self.cleaned_data["storename"] == "":
                    raise forms.ValidationError('Store name is required for champion.')
                elif (' ' in self.cleaned_data["storename"]) == True:
                    raise forms.ValidationError('Please choose storename without unnecessary spaces.')
                else:
                    return self.cleaned_data['storename']