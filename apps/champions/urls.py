from django.conf.urls import patterns, url
from views import *
from oscar.apps.promotions.views import *
from django.views.generic import TemplateView
from django.contrib.auth.decorators import login_required

urlpatterns = patterns("",
# follow functionality URLs
    url(r'^review_add/$', ProductReviewCreate.as_view(), 
                                                    name="review_add_popup"),
    url(r'^champion_follow/$', ChampionFollow.as_view(),
                                                    name="champion_follows"),
    url(r'^champion_following/$', ChampionFollowing.as_view(),
                                                    name="champion_following"),
    url(r'^follow_or_unfollow/$', ChampionFollowOrUnfollow.as_view(),
                                                    name="follow_or_unfollow"),   
    
    url(r'^store_image_change/$', StoreImageChange.as_view(), name='store-image-change'),
    url(r'^store_timeline_change/$', StoreTimelineChange.as_view(), name='store-timeline-change'),
    url(r'^deposit-form-submit/$', DepositeFormView.as_view(), name='deposit-form-submit'),    
    
#static URLs
    url(r'^about_us/$', TemplateView.as_view(template_name="static_pages/about_us.html"), name="about_us"),
    url(r'^faq/$', 
            TemplateView.as_view(template_name="static_pages/faq.html")
                                                        , name="faq"),
    url(r'^return_policy/$', 
            TemplateView.as_view(template_name="static_pages/return_policy.html")
                                                        , name="return_policy"),
    url(r'^terms_of_use/$', 
            TemplateView.as_view(template_name="static_pages/terms_of_use.html")
                                                        , name="terms_of_use"),
    url(r'^privacy_policy/$', 
            TemplateView.as_view(template_name="static_pages/privacy_policy.html")
                                                        , name="privacy_policy"),
    url(r'^how_it_works/$', 
            TemplateView.as_view(template_name="static_pages/how_it_works.html")
                                                        , name="how_it_works"),
    #Payment Info page
    url(r'^payment_info/$', 
            TemplateView.as_view(template_name="dashboard/payment_info.html")
                                                        , name="payment_info"),
    url(r'^payment_info_submit/$', PaymentInfo.as_view(), name="payment_info_submit"),
    
#    Champion Dashboard

    
#     
    url(r'^dashboard/champion-license-delete/(?P<pk>\d+)$',
                            DeleteIndorsment.as_view(), name="license-delete"),
    url(r'^dashboard/account/$', 
        login_required(TemplateView.as_view(template_name="champions/dashboard/account.html"))
                                                , name="champion-account"),
    url(r'^dashboard/transactions/$', 
        login_required(TemplateView.as_view(template_name="champions/dashboard/champion_transactions.html"))
                                                , name="champion-transactions"),
                                                
#    store details

    url(r'^(?P<storename>[\w ]+)/$', StoreDetail.as_view(), name='store-details'),
    url(r'^(?P<storename>[\w ]+)/(?P<product_slug>[\w-]*)_(?P<pk>\d+)/$', 
                                    ChampionsProductDetail.as_view(),
                                    name='champion-product-detail'),
                                    
    url(r'^(?P<storename>[\w ]+)/followings/$',
                        login_required(MyFollows.as_view()), name="my-follows"),
    url(r'^(?P<storename>[\w ]+)/followers/$',
                    login_required(MyFollowers.as_view()), name="my-followers"),
                    
    url(r'^(?P<storename>[\w ]+)/earnings/$',
                    login_required(MyEariningsView.as_view()), name="my-earnings"),                

    url(r'^products-indorse/$', login_required(IndorseProducts.as_view()), name="prodcut-indorse"),
    url(r'^products-indorse/(?P<category_slug>[\w-]+(/[\w-]+)*)/$',
                            IndorseProducts.as_view(),
                            name="prodcut-indorse-catalogue"),
    url(r'^indorse-detail/(?P<pk>\d+)/$', IndorseProductDetail.as_view(),
                            name="product-indorse-detail"),


    url(r'^product-buy/$', login_required(BuyProducts.as_view()), name="product-buy"),
    url(r'^user-dashboard/$', login_required(AllTransactionslistView.as_view()), name="champion-dashboard"),

    url(r'^product-buy/(?P<category_slug>[\w-]+(/[\w-]+)*)/$',
                            BuyProducts.as_view(),
                            name="prodbuy-category"), 
    url(r'^#popup$', HomeView.as_view(), name='home_login'),
)