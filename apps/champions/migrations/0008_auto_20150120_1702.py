# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('champions', '0007_auto_20150114_1114'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='championlicense',
            name='champion',
        ),
        migrations.RemoveField(
            model_name='championlicense',
            name='product',
        ),
        migrations.DeleteModel(
            name='ChampionLicense',
        ),
    ]
