# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('userprofile', '0003_userprofile_location'),
    ]

    operations = [
        migrations.CreateModel(
            name='ChampionLicense',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('no_of_license', models.PositiveIntegerField(verbose_name='quantity')),
                ('date_created', models.DateTimeField(auto_now_add=True, verbose_name='Date created')),
                ('champion', models.ForeignKey(related_name=b'licenses', to='userprofile.UserProfile')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
    ]
