# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('champions', '0013_auto_20150211_0855'),
    ]

    operations = [
        migrations.AlterField(
            model_name='championreview',
            name='time',
            field=models.DateTimeField(auto_now_add=True, verbose_name='date joined', db_index=True),
        ),
    ]
