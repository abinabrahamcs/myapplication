# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('userprofile', '0007_auto_20150109_1755'),
        ('champions', '0005_auto_20141218_1904'),
    ]

    operations = [
        migrations.CreateModel(
            name='ChampionPayment',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('commision_price', models.DecimalField(default=0, verbose_name='payment Price', max_digits=10, decimal_places=2)),
                ('paypal_reff_id', models.CharField(max_length=200, verbose_name='paypal reference id')),
                ('date', models.DateTimeField(verbose_name='Payment date')),
                ('champion', models.ForeignKey(related_name=b'my_payments', to='userprofile.UserProfile')),
                ('user', models.ForeignKey(related_name=b'Paying user', to='userprofile.UserProfile')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
    ]
