# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('champions', '0008_auto_20150120_1702'),
    ]

    operations = [
        migrations.CreateModel(
            name='BulkProductRange',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('quantity_from', models.PositiveIntegerField(verbose_name='Quantity From')),
                ('quantity_to', models.PositiveIntegerField(null=True, verbose_name='Quantity To', blank=True)),
                ('price', models.DecimalField(verbose_name='Price', max_digits=12, decimal_places=2)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
    ]
