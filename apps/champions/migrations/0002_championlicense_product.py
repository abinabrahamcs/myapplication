# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('catalogue', '0002_product_innovator'),
        ('champions', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='championlicense',
            name='product',
            field=models.ForeignKey(default=21, to='catalogue.Product'),
            preserve_default=False,
        ),
    ]
