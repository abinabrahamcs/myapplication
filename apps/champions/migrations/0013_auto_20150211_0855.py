# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('champions', '0012_auto_20150210_1606'),
    ]

    operations = [
        migrations.AlterField(
            model_name='championreview',
            name='time',
            field=models.DateTimeField(auto_now_add=True, verbose_name='date joined'),
        ),
    ]
