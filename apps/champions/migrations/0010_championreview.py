# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('userprofile', '0011_auto_20150119_1256'),
        ('catalogue', '0006_product_bulk_range'),
        ('champions', '0009_bulkproductrange'),
    ]

    operations = [
        migrations.CreateModel(
            name='ChampionReview',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('review', models.TextField(verbose_name='Product Review')),
                ('champion', models.ForeignKey(related_name=b'champion_reviews', to='userprofile.UserProfile')),
                ('product', models.ForeignKey(related_name=b'product_reviews_by_champion', to='catalogue.Product')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
    ]
