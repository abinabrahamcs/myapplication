# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('champions', '0014_auto_20150211_1250'),
    ]

    operations = [
        migrations.AlterField(
            model_name='championreview',
            name='time',
            field=models.DateTimeField(auto_now=True, verbose_name='date joined'),
        ),
    ]
