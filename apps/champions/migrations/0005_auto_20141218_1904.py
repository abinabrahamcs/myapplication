# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('champions', '0004_auto_20141218_1900'),
    ]

    operations = [
        migrations.AlterField(
            model_name='championlicense',
            name='num_allocated',
            field=models.PositiveIntegerField(default=1, null=True, verbose_name='Number of allocated licenses', blank=True),
        ),
    ]
