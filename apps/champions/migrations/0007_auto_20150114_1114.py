# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('champions', '0006_championpayment'),
    ]

    operations = [
        migrations.AlterField(
            model_name='championlicense',
            name='num_allocated',
            field=models.PositiveIntegerField(default=0, null=True, verbose_name='Number of allocated licenses', blank=True),
        ),
        migrations.AlterField(
            model_name='championpayment',
            name='user',
            field=models.ForeignKey(related_name=b'Paying_user', to='userprofile.UserProfile'),
        ),
    ]
