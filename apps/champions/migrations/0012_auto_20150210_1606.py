# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('champions', '0011_championreview_rime'),
    ]

    operations = [
        migrations.RenameField(
            model_name='championreview',
            old_name='rime',
            new_name='time',
        ),
    ]
