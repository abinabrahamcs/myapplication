# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import django_countries.fields


class Migration(migrations.Migration):

    dependencies = [
        ('champions', '0015_auto_20150211_0732'),
    ]

    operations = [
        migrations.CreateModel(
            name='PaymentOptions',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('country', django_countries.fields.CountryField(max_length=2)),
                ('payment_option', models.CharField(default=b'OPTION1', max_length=20, choices=[(b'OPTION1', 'Email Transfer'), (b'OPTION2', 'Bank Transfer')])),
            ],
            options={
            },
            bases=(models.Model,),
        ),
    ]
