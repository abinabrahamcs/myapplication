# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('champions', '0002_championlicense_product'),
    ]

    operations = [
        migrations.AlterField(
            model_name='championlicense',
            name='product',
            field=models.ForeignKey(related_name=b'indorsed_products', to='catalogue.Product'),
        ),
    ]
