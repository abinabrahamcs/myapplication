# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('champions', '0003_auto_20141128_1801'),
    ]

    operations = [
        migrations.AddField(
            model_name='championlicense',
            name='num_allocated',
            field=models.PositiveIntegerField(default=1, verbose_name='Number of allocated licenses'),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='championlicense',
            name='no_of_license',
            field=models.PositiveIntegerField(verbose_name='Number of indorsed licenses'),
        ),
    ]
