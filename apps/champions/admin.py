from django.contrib import admin
from models import *

admin.site.register(BulkProductRange)
admin.site.register(ChampionReview)
admin.site.register(PaymentOptions)