from django.db import models
from apps.userprofile.models import *
from django.utils.translation import ugettext_lazy as _
from oscar.core.loading import get_class, get_model
from django.utils import timezone
from django_countries.fields import CountryField
  
class ChampionPayment(models.Model):
    """
    Champion payment details
    """
    champion = models.ForeignKey(UserProfile, related_name="my_payments")    
    commision_price = models.DecimalField(_("payment Price"), 
                                    default=0, decimal_places=2, max_digits=10)
    user = models.ForeignKey(UserProfile, related_name="Paying_user")
    paypal_reff_id = models.CharField(_("paypal reference id"), max_length=200)
    date = models.DateTimeField(_("Payment date"))
    
    def __unicode__(self):
        return self.champion.user.first_name
    
    
class BulkProductRange(models.Model):
    """
    Bulk product range 
    
    """
    
    quantity_from = models.PositiveIntegerField(_('Quantity From'))
    quantity_to = models.PositiveIntegerField(_('Quantity To'), blank=True, null=True)
    price = models.DecimalField(_('Price'), decimal_places=2, max_digits=12)
    
    def __unicode__(self):
        return str(self.quantity_from)+" - "+str(self.quantity_to)  


class ChampionReview(models.Model):
    """
    champions product reviews
    """
    champion = models.ForeignKey(UserProfile, related_name="champion_reviews")
    product = models.ForeignKey('catalogue.Product', related_name="product_reviews_by_champion")
    review = models.TextField(_('Product Review'))
    time = models.DateTimeField(_('date joined'), auto_now=True)
    
    def __unicode__(self):
        return str(self.champion)+" - "+str(self.product)
    


class PaymentOptions(models.Model):
    """
    Payment Options depends country
    """
    OPTIONS = (
        ('OPTION1', _('Email Transfer')),
        ('OPTION2', _('Bank Transfer')),
    )
    
    country = CountryField()
    payment_option = models.CharField(max_length=20,
                                      choices=OPTIONS,
                                      default='OPTION1')

    
    def __unicode__(self):
        return self.country.code