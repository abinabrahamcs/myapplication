from django_countries import countries

Country = get_model('address', 'Country')

class Command(BaseCommand):
    """
    Synchronize countries.
    """
    def handle(self, *args, **options):
        count = 1
        for key in dict(countries):
            count += 1
            try:
                country = Country(iso_3166_1_a2=key, printable_name=dict(countries)[key], display_order=count, is_shipping_country=True)
                country.save()
            except Exception as e:
                pass
