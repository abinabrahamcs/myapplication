import json
import datetime
from django.shortcuts import render
from django.views.generic import ListView, View
from django.views.generic.detail import DetailView
from django.views.generic.edit import FormView
from oscar.core.loading import get_class, get_model
from django.utils.translation import ugettext_lazy as _
from django.core.urlresolvers import reverse, reverse_lazy
from django.contrib import messages
from django import forms
from django.shortcuts import render_to_response
from django.template import RequestContext

from apps.userprofile.models import *
from apps.champions.models import *

from django.http import HttpResponse
from django.shortcuts import redirect, get_object_or_404
from django.views.generic.edit import DeleteView
from endless_pagination.views import AjaxListView
from django.http import HttpResponseRedirect
from django.http import Http404
from django.views.decorators.csrf import csrf_exempt
from django.views.generic import TemplateView
from django.contrib.auth.models import User
from models import PaymentOptions


Selector = get_class('partner.strategy', 'Selector')
strategy = Selector().strategy()

Product = get_model('catalogue', 'product')
ProductReview = get_model('reviews', 'ProductReview')
OrderLine = get_model('order', 'Line')
Country = get_model('address', 'Country')

Category = get_model('catalogue', 'category')
get_product_search_handler_class = get_class(
    'catalogue.search_handlers', 'get_product_search_handler_class')
ProductReview = get_model('reviews', 'ProductReview')

    
class ChampionsProductDetail(DetailView):
    """
    Product detail page
    """
    model = Product
    template_name = "catalogue/champion-detail.html"
    
    def get(self, request, *args, **kwargs):
        try:
            champion = UserProfile.objects.get(storename=kwargs['storename'])
        except:
            champion = None
        self.champion = champion    
        return super(ChampionsProductDetail, self).get(request, *args, **kwargs)
    
    def get_context_data(self, **kwargs):
        context = super(ChampionsProductDetail, self).get_context_data(**kwargs)
        context['champion'] = self.champion
        context['reviews'] = self.get_reviews()
        return context    
    
    def get_reviews(self):
        return self.object.reviews.filter(status=ProductReview.APPROVED)
    
    
class StoreDetail(AjaxListView):
    """
    Store detail page
    """
    template_name = "champions/store-detail.html"
    page_template = 'champions/store_products_list.html'
    
    def get_queryset(self):
        champion = self.get_champion_object()
        licenses = None        
        return licenses
    
    def get_champion_object(self):
        try:
            champion = UserProfile.objects.get(storename=self.kwargs['storename'])
        except: 
            champion = None
        return champion
    
    def get_context_data(self, **kwargs):
        context = super(StoreDetail, self).get_context_data(**kwargs)  
        context['champion'] = self.get_champion_object()
        champion = self.get_champion_object()
        if champion.follows.all():     
            user_list = champion.follows.all().values_list('user',flat=True)
        else:
            user_list = []
        if self.request.user.id in user_list:
            status = 0
        else:
            status = 1
        context['status'] = status
        reviews_list = []
        if champion:  
            reviews = champion.champion_reviews.all()
            reviews = reviews.order_by('-time')
            for review in reviews:
                available_countries = list(review.product.available_countries.all())
                if available_countries:
                    if self.request.country_object in available_countries:
                       reviews_list.append(review)
                else:    
                    reviews_list.append(review)
        else:
            reviews_list = None
            
            
        context['reviews'] = reviews_list
        return context
    
    def get(self, request, *args, **kwargs):
        """
        Redirect to page not found
        """
        if not self.get_champion_object():
             raise Http404
        return super(StoreDetail, self).get(request, *args, **kwargs)
                            
                            
class IndorseProducts(AjaxListView):
    """
    List products for indorse
    """
    model = Product
    context_object_name = "products"
    template_name = "champions/product_indorse.html"
    page_template = 'champions/product_indors_list.html'
    
    def get_queryset(self):    
        sort_by = self.request.GET.get('sort_by')
        if 'category_slug' in self.kwargs:
            filters = {'slug': self.kwargs['category_slug']}
            category = get_object_or_404(Category, **filters)
            qs = super(IndorseProducts, self).get_queryset()\
                                        .filter(categories=category).exclude(stockrecords__available_date__lte=datetime.date.today())
        else:
            qs = super(IndorseProducts, self).get_queryset().exclude(stockrecords__available_date__lte=datetime.date.today()) 
            
        if sort_by == 'new':
            qs = qs.order_by('-date_updated')
        elif sort_by == 'popular':
            qs = qs.order_by( '-stats__num_views')         
        elif sort_by == 'price':
            qs = qs.order_by('-stockrecords__price_excl_tax')
       
        qs = list(qs)
        if self.request.user.is_authenticated:
            product_list = []
            for product in qs:
                info = strategy.fetch_for_product(product)
                if product not in list(self.request.user.user_profile.champion_licenses.all()) and info.availability.is_available_to_buy:
                    product_list.append(product)                   
            qs = product_list
        if sort_by == "indorsement":
                qs = sorted(product_list, key=lambda x: x.total_indorsed_license, reverse=True)
        elif sort_by == 'earnings':            
                qs = sorted(product_list, key=lambda x: x.champion_earn, reverse=True)
        elif sort_by == 'days-to-go':
                qs = sorted(product_list, key=lambda x: x.days_to_go)
        return qs
    
class IndorseProductDetail(DetailView):
    """
    Product detail page for indorse
    """
    model = Product
    template_name = "champions/product_indorse_detail.html"    
    
    def get_context_data(self, *args, **kwargs):
        context = super(IndorseProductDetail, self).get_context_data(*args, **kwargs)
        context['reviews'] = self.get_reviews()
        return context
    
    def post(self, request, *args, **kwargs):
        error = False
        try:
            product_object = Product.objects.get(id=kwargs['pk'])
        except:
            messages.error(request, _('The product object not valid.'))
            product_object = None
        
        try:
            champion_object = UserProfile.objects.get(id=request.POST['champion'])
        except:
            messages.error(request, _('The user Object not valid.'))
            champion_object = None
        if product_object and champion_object:
            info = strategy.fetch_for_product(product_object)
            if not info.availability.is_available_to_buy:
                msg = info.availability.message
                messages.error(request, _(msg))
                error =True
            elif champion_object in product_object.champions.all():
                messages.error(request, _('%s is already indorsed this product'% champion_object))
                error =True
            elif not product_object.is_product_expired:
                messages.error(request, _('The License of %s is expired.'% product_object))
                error = True
            elif not product_object.has_license:
                messages.error(request, _('50 Licenses of %s is already indorsed'))
                error = True
            elif not champion_object.champions_exceeded_quota:
                messages.error(request, _('%s already indorsed 25 products')% champion_object)
                error = True
                
        if not error:
            product_object.champions.add(champion_object)
            try:
                champion_review = ChampionReview.objects.get(champion=champion_object, product=product_object)
            except:
                champion_review = ChampionReview()
                champion_review.champion = champion_object
                champion_review.product = product_object
            champion_review.review = request.POST['review']
            champion_review.time =  datetime.datetime.now()
            champion_review.save()    
            
            messages.success(request, _('successfully indorsed %s')% product_object,)
            return redirect('store-details', storename=self.request.user.user_profile.storename)
        else:
            return render_to_response(self.template_name,
                      {"object": self.get_object(),"reviews": self.get_object().reviews.filter(status=ProductReview.APPROVED)},                      
                      context_instance=RequestContext(self.request))
                      
    def get_reviews(self):
        return self.object.reviews.filter(status=ProductReview.APPROVED)
    
    
class ChampionFollow(View):
    """
    champion updation
    """

    def post(self,request):        
        data = {}
        champion_id = request.POST.get('champ_id')
        try:
            champion_profile = UserProfile.objects.get(id=champion_id)
        except:
            champion_profile = None
            
        try:
            user_profile = UserProfile.objects.get(id=request.user.user_profile.id)
        except:
            user_profile = None            
            
        my_follow_list = user_profile.follows.all().values_list('user',flat=True)
        if champion_profile.user.id in my_follow_list:
            user_profile.follows.remove(champion_profile)
            messages.success(request, _('successfully UnFollowed %s')% user_profile,)
            data['status_su'] = 'unfollowed'
        else:
            user_profile.follows.add(champion_profile)
            messages.success(request, _('successfully Followed %s')% user_profile,)
            data['status_su'] = 'followed'
        if request.is_ajax():
            return HttpResponse(json.dumps(data))
        else:            
            return HttpResponseRedirect(request.META.get('HTTP_REFERER','/'))
    
    
class ChampionFollowOrUnfollow(View):
    """
    champion follow
    """

    def post(self,request):        
        data = {}
        champion_id = request.POST.get('champ_id')
        
        try:
            champion_profile = UserProfile.objects.get(id=champion_id)
        except:
            champion_profile = None
        
        try:
            user_profile = UserProfile.objects.get(id=champion_id)
        except:
            user_profile = None            
    
        my_follow_list = user_profile.follows.all().values_list('user',flat=True)
        if champion_profile.user.id in my_follow_list:
            data['status_su'] = 'followed'
        else:
            data['status_su'] = 'unfollowed'
        return HttpResponse(json.dumps(data))
    
class PaymentInfo(View):
    """
    champion paymentInfo
    """

    def post(self,request):        
        data = {}
        try:
            email = request.POST.get('email')
        except:
            email = None
        try:
            c_password = request.POST.get('c_password')
        except:
            c_password = None
        try:
            user_profile = UserProfile.objects.get(user=request.user)
        except:
            user_profile = None
        if not request.user.check_password(c_password):
            data['status_err'] = 'Check your password!'
        else:
            data['status_suc'] = 'Successfully updated'
            user_profile.paypal_email = email
            user_profile.save()
        data['email']=email
        return HttpResponse(json.dumps(data))
    

class BuyProducts(AjaxListView):
    """
    List products that can buy by the enduser.
    """
    model = Product
    context_object_name = "products"
    template_name = "buy/browse.html"
    page_template = "buy/product_buy_list.html"

    def get_queryset(self):
        
        sort_by = self.request.GET.get('sort_by')        
        if 'category_slug' in self.kwargs:
            filters = {'slug': self.kwargs['category_slug']}
            category = get_object_or_404(Category, **filters)
            reviews = ChampionReview.objects.filter(product__categories=category)
        else:    
            reviews = ChampionReview.objects.all()
        if sort_by == 'price':
            reviews = reviews.order_by('-product__stockrecords__price_excl_tax')
        elif sort_by == 'new':
            reviews = reviews.order_by('-time')
        else:    
            reviews = reviews
        print 'sort_bysort_by',self.kwargs
        review_list = []
        products_list = []
        review_user_list = []
        
        for product in reviews:
            review_user_list.append(product.champion)
            
        if self.request.user.is_authenticated():
            intersected_list = set(review_user_list).intersection(self.request.user.user_profile.follows.all())
            
        for item in reviews:
            info = strategy.fetch_for_product(item.product)
            available_countries = list(item.product.available_countries.all())
            if item.champion in intersected_list and item.product not in products_list and info.availability.is_available_to_buy:
                if available_countries:
                    if self.request.country_object in available_countries:
                        products_list.append(item.product)
                        review_list.append(item)
                else:        
                    products_list.append(item.product)
                    review_list.append(item)
                
        if sort_by == 'indorsements':
            review_list = sorted(review_list, key=self.get_indorsed_license, reverse=True)
            
        return review_list
    
    def get_indorsed_license(self, item):
        
        return item.product.total_indorsed_license
    
    
class MyFollowers(AjaxListView):
    """
    Listing the followers
    """
    model = UserProfile
    template_name = "champions/dashboard/myfollowers.html"
    page_template = "champions/dashboard/followers_list.html"
    context_object_name = "followers"
    
    def get_queryset(self):
        champion = self.get_champion_object()
        followers = champion.follower.all()
        return followers
    
    def get_champion_object(self):
        try:
            champion = UserProfile.objects.get(storename=self.kwargs['storename'])
        except: 
            champion = None
        return champion
    
    def get_context_data(self, **kwargs):
        context = super(MyFollowers, self).get_context_data(**kwargs)
        context['champion'] = self.get_champion_object()
        return context
    
    
class MyFollows(AjaxListView):
    """
    Listing the follows
    """
    model = UserProfile
    template_name = "champions/dashboard/myfollows.html"
    page_template = "champions/dashboard/follows_list.html"
    context_object_name = "follows"
    
    def get_queryset(self):        
        champion = self.get_champion_object()
        follows = champion.follows.all()
        return follows
    
    def get_champion_object(self):
        try:
            champion = UserProfile.objects.get(storename=self.kwargs['storename'])
        except: 
            champion = None
        return champion
    
    def get_context_data(self, **kwargs):
        context = super(MyFollows, self).get_context_data(**kwargs)
        context['champion'] = self.get_champion_object()
        return context
    
class DeleteIndorsment(View):
    """
    Delete champion license
    """
    model = Product
    success_url = reverse_lazy('champion-dashboard')
    
    def post(self, request, *args, **kwargs):
        try:
            product_object = Product.objects.get(id=kwargs['pk'])
        except:
            product_object = None
        if product_object:
            product_object.champions.remove(request.user.user_profile)
            messages.success(request, _('successfully deleted license of the product %s')% product_object,)
        return redirect('champion-dashboard')
    
class ChampionFollowing(View):
    """
    champion updation
    """

    def post(self,request):        
        data = {}
        champion_id = request.POST.get('champ_id')
        
        try:
            champion_profile = UserProfile.objects.get(id=champion_id)
        except:
            champion_profile = None
            
        try:
            user_profile = UserProfile.objects.get(id=champion_id)
        except:
            user_profile = None
            
        my_follow_list = user_profile.follows.all().values_list('user',flat=True)
        
        if champion_profile.user.id in my_follow_list:
            user_profile.follows.remove(champion_profile)
            data['status_su'] = 'unfollowed'
        else:
            user_profile.follows.add(champion_profile)
            data['status_su'] = 'followed'
            
        return HttpResponse(json.dumps(data))
    
    
class StoreImageChange(View):
    """
    Change store profile picture
    """
    def post(self, request):
        try:
            champion_profile = UserProfile.objects.get(id=request.POST['champion'])
        except:
            champion_profile = None
        try:
            profileimage = request.FILES['image']
        except:
            profileimage = None
        print "profileimageprofileimage",profileimage
        if profileimage:
            if champion_profile:    
                champion_profile.image = profileimage
                champion_profile.save()
                messages.success(request, _('successfully updated %s image')% champion_profile.user,)
            else:
                messages.error(request, _('Not updated image'))

        return HttpResponseRedirect('/'+request.user.user_profile.storename+'/')    
   

class StoreTimelineChange(View):
    """
    Change store timeline photo
    """

    def post(self, request):
        print request.FILES
        data ={}
        try:
            champion_profile = UserProfile.objects.get(id=request.POST['champion']) 
            user_obj = User.objects.get(id=request.user.id)
        except:
            champion_profile = None
            user_obj = None
        try:
            user_bio = request.POST.get('userbio')
        except:
            user_bio = None
        print "user_biouser_bio",user_bio
        try:
            storename = request.POST.get('username')
        except:
            storename = None
        try:
            coverphoto = request.FILES['coverphoto']
        except:
            coverphoto = None
        try:
            link1 = request.POST.get('link1')
        except:
            link1 = None
        try:
            link2 = request.POST.get('link2')
        except:
            link2 = None
        if storename:
            champion_profile.storename = storename
            champion_profile.save()

        champion_profile.bio = user_bio
        champion_profile.save()

        champion_profile.link1 = link1
        champion_profile.link2 = link2
        champion_profile.save()
        
        if coverphoto:
            if champion_profile:    
                champion_profile.timeline_image = request.FILES['coverphoto']
                champion_profile.save()
                data['success'] = True
                data['message'] = 'successfully updated %s timeline photo' % champion_profile.user
            else:
                data['success'] = False
                data['message'] = 'Not updated timeline photo'
        return HttpResponse(json.dumps(data), content_type="application/json")

   
class ProductReviewCreate(View):
    """
    champion follow
    """

    def post(self,request):        
        data = {}
        product = get_object_or_404(
            Product, pk=request.POST.get('p_pk'))
        if not product.is_review_permitted(request.user):
            if product.has_review_by(request.user):
                message = _("You have already reviewed this product!")
            else:
                message = _("You can't leave a review for this product.")
           
        product_review = ProductReview()
        product_review.user = request.user
        product_review.product = product
       # product_review.title = request.POST['title']
        product_review.body = request.POST['review']
        product_review.date_created =  datetime.datetime.now()
        product_review.save() 
        message = _("You are successfully Reviewed")
        messages.warning(self.request, message)
            
        return HttpResponse(json.dumps(data))


class MyEariningsView(TemplateView):
    """
    My earnings page
    """
    
    template_name = "champions/dashboard/earning.html"
    
    def get_champion_object(self):
        try:
            champion = UserProfile.objects.get(storename=self.kwargs['storename'])
        except: 
            champion = None
        return champion
    
    def get_payment_options(self):
        try:
            payment_opts = PaymentOptions.objects.all()
        except:
            payment_opts = None
        return payment_opts
            
    
    def get_context_data(self, **kwargs):
        context = super(MyEariningsView, self).get_context_data(**kwargs)
        context['champion'] = self.get_champion_object()
        context['payment_opts'] = self.get_payment_options()
        return context
    
    
class DepositeFormView(View):
    """
    Deposite form submit
    """
    
    def post(self, request):
        depositeform = DepositeForm()
        depositeform.user_profile = request.user.user_profile
        depositeform.file = request.FILES['file']
        depositeform.save()
        messages.success(request, _('successfully uploaded file'))
        return redirect('my-earnings', storename=self.request.user.user_profile.storename)
    
    
class AllTransactionslistView(AjaxListView):
    """
    Listing current user all the transactions
    """
    model = UserProfile
    template_name = "champions/dashboard/dashboard.html"
    page_template = "champions/dashboard/dashboard_list.html"
    context_object_name = "products"
    
    def get_queryset(self):
        products = self.request.user.user_profile.champion_licenses.all()
        return products