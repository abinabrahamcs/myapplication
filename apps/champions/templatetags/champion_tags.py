from django import template
from oscar.core.loading import get_class, get_model
from decimal import *

register = template.Library()

OrderLine = get_model('order', 'Line')
UserProfile = get_model('userprofile', 'UserProfile')
ChampionReview = get_model('champions', 'ChampionReview')

@register.filter
def unit_sold(product, champion):
    """
    Get total units of a product sale by a champion
    """
    count = 0
    orders = OrderLine.objects.filter(champion=champion, product=product)
    if orders:
        for license in orders:
            count+=license.quantity      
    return count


@register.filter
def total_earnings(product, champion):
    """
    Get total earnings from a particular product of the champion
    """
    price = Decimal(0)
    orders = OrderLine.objects.filter(champion=champion, product=product)
    if orders:
        for license in orders:
            price+=license.get_order_earn
    return price

@register.filter
def get_champions_list(user):
    """
    Listing champions
    """
    list = []
    try:
        user_profile = user.user_profile.id
    except:
        user_profile = None
    champion_list = UserProfile.objects.all()
    for champion in champion_list:        
        if champion.champion_licenses.all().count() > 0:
            if not user_profile in champion.follower.all().values_list('id',flat=True):
                list.append(champion)
    return list


@register.filter
def get_product_champion(product, user_profile):
    """
    Get a product champion
    """    
    follows_champion_products = set(product.champions.all()).intersection(user_profile.follows.all())
    object = list(follows_champion_products)
    if object:
        champ_obj = object[0]
    else:
        champ_obj = None
    return champ_obj


@register.filter
def get_product_champion_review(product, champion):
    """
    Get a product champion review
    """
    try:
        reviews = ChampionReview.objects.get(champion_id=champion, product_id=product.id)
        reviews = reviews.review
    except:
        reviews = None
    return reviews

@register.filter
def get_product_champion_review_date_time(product, champion):
    """
    Get a product champion review
    """
    from datetime import datetime, date, timedelta
    today = datetime.now().replace(tzinfo=None)
    try:
        reviews = ChampionReview.objects.get(champion_id=champion, product_id=product.id)
        review_time = reviews.time.replace(tzinfo=None)
        review_time.replace(tzinfo=None)
        time = today - review_time
        if time:
            if time.days:
                label = "<label>%s %s</label> <span>%s days ago</span>" % (reviews.time.strftime('%b'), reviews.time.strftime('%d'), time.days)
            elif time.seconds >= 3600:
                label = "<label>%s %s</label> <span>%s hours ago</span>" % (reviews.time.strftime('%b'), reviews.time.strftime('%d'), time.seconds//3600)
            elif time.seconds >= 60:
                label = "<label>%s %s</label> <span>%s minutes ago</span>" % (reviews.time.strftime('%b'), reviews.time.strftime('%d'), time.seconds//60)
            else:       
                label = "<label>%s %s</label>" % (reviews.time.strftime('%b'), reviews.time.strftime('%d'))    
        else:       
            label = "<label>%s %s</label>" % (reviews.time.strftime('%b'), reviews.time.strftime('%d'))
            
    except Exception as e:
        label = None
    return label

@register.filter
def check_followed(champion, user):
    """
    Check authenticated user is followed this champion
    """
    try:
        champion_profile = UserProfile.objects.get(id=champion)
    except:
        champion_profile = None
    
    try:
        user_profile = UserProfile.objects.get(id=user)
    except:
        user_profile = None       
    my_follow_list = user_profile.follows.all().values_list('user',flat=True)
    if champion_profile.user.id in my_follow_list:
        status = True
    else:
        status = False
    return status
    
    
    