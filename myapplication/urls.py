from django.conf.urls import include, url, patterns
from oscar.app import application
from django.contrib import admin
from django.conf import settings

urlpatterns = patterns('',
    (r'^social_accounts/', include('allauth.urls')),
    url(r'^i18n/', include('django.conf.urls.i18n')),
    url(r'^checkout/paypal/', include('paypal.express.urls')),    
    url(r'^ckeditor/', include('ckeditor.urls')),
    url(r'^admin/', include(admin.site.urls)),
    url(r'', include(application.urls)),
    url(r'^contact/', include('apps.contactus.urls')),
    url(r'', include('apps.userprofile.urls')),
    url(r'', include('apps.champions.urls')),
)

urlpatterns = patterns('',
    url(r'^media/(?P<path>.*)$', 'django.views.static.serve',
    {'document_root': settings.MEDIA_ROOT,}),
    url(r'', include('django.contrib.staticfiles.urls')),

) + urlpatterns

if settings.DEBUG is False:   #if DEBUG is True it will be served automatically
    urlpatterns += patterns('',
            url(r'^static/(?P<path>.*)$', 'django.views.static.serve', 
                    {'document_root': settings.STATIC_ROOT}),
    )
    