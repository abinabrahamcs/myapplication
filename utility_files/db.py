from django.db import models
from django.utils.translation import ugettext_lazy as _

from django_extensions.db.fields import (ModificationDateTimeField,
                                        CreationDateTimeField)

class DateBaseModel(models.Model):
    """
    Base model that provides:
        * self managed created field
        * self managed modified field
    """
    created = CreationDateTimeField(_('created'))
    modified = ModificationDateTimeField(_('modified'))

    class Meta:
        get_latest_by = 'modified'
        ordering = ('-modified', '-created',)
        abstract = True